#include "Input.h"
#include <iostream>

namespace input
{

    Input checkInput()
    {
        Input newInput;
        newInput.currentMousePosition = GetMousePosition();
        if (IsMouseButtonPressed(MouseButton::MOUSE_LEFT_BUTTON)) { newInput.leftClick_pressed = true; }
        if (IsMouseButtonPressed(MouseButton::MOUSE_MIDDLE_BUTTON)) { newInput.middleClick_pressed = true; }
        if (IsMouseButtonPressed(MouseButton::MOUSE_RIGHT_BUTTON)) { newInput.rightClick_pressed = true; }
        if (IsMouseButtonDown(MouseButton::MOUSE_LEFT_BUTTON)) { newInput.leftClick_down = true; }
        if (IsMouseButtonDown(MouseButton::MOUSE_MIDDLE_BUTTON)) { newInput.middleClick_down = true; }
        if (IsMouseButtonDown(MouseButton::MOUSE_RIGHT_BUTTON)) { newInput.rightClick_down = true; }
        if (IsMouseButtonReleased(MouseButton::MOUSE_LEFT_BUTTON)) { newInput.leftClick_released = true; }
        if (IsMouseButtonReleased(MouseButton::MOUSE_MIDDLE_BUTTON)) { newInput.middleClick_released = true; }
        if (IsMouseButtonReleased(MouseButton::MOUSE_RIGHT_BUTTON)) { newInput.rightClick_released = true; }
        return newInput;
    }

    void updateInputGame()
    {
        currentInput = checkInput();
    }

    Input currentInput;

}