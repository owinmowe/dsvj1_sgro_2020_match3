#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace input
{

	struct Input
	{

		bool rightClick_pressed = false;
		bool middleClick_pressed = false;
		bool leftClick_pressed = false;
		bool rightClick_down = false;
		bool middleClick_down = false;
		bool leftClick_down = false;
		bool rightClick_released = false;
		bool middleClick_released = false;
		bool leftClick_released = false;
		Vector2 currentMousePosition { 0, 0 };

	};

	void updateInputGame();
	extern Input currentInput;

}

#endif 