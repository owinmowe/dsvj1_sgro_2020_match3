#ifndef HEALTH_BAR_H
#define HEALTH_BAR_H

#include "raylib.h"

namespace ui
{
	namespace in_game
	{
		class HealthBar
		{
		public:
			HealthBar();
			void draw();
			void setRectangle(Rectangle rec);
			Rectangle getRectangle();
			void setOverlayTexture(Texture2D* tex);
			Texture2D* getOverlayTexture();
			void setBarTexture(Texture2D* tex);
			Texture2D* getBarTexture();
			void setCurrentElementTexture(Texture2D* tex);
			Texture2D* getCurrentElementTexture();
			void updateBar(float currentLife, float maxLife);
		private:
			Rectangle overlayRectangle = { 0,0,0,0 };
			Rectangle barRectangle = { 0,0,0,0 };
			float maxValueWidth = 0;
			Rectangle elementRectangle = { 0,0,0,0 };
			Texture2D* overlayTexture = nullptr;
			Texture2D* barTexture = nullptr;
			Texture2D* currentElementTexture = nullptr;
		};
	}
}
#endif // !HEALTH_BAR_H
