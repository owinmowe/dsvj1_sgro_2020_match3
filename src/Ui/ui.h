#ifndef UI_H
#define UI_H

#include "raylib.h"
#include <string>

namespace ui
{

	class GUIcomponent
	{
	public:
		bool selected = false;
		bool active = false;
		virtual void update() = 0;
		virtual void action() = 0;
		virtual void drawComponent() = 0;
		void setRectangle(Rectangle rec);
		Rectangle getRectangle();
	private:
		Rectangle collider = { 0,0,0,0 };
	};
	void mouseHoverClickLogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	void drawAllComponents(const int GUI_SIZE, GUIcomponent* GUI_Ptr[]);
	const float TEXT_SIZE_SCALE = .02f;
	const Color GUI_SELECTED_COLOR = RED;
	const Color GUI_NOT_SELECTED_COLOR = WHITE;

	class Button : public GUIcomponent
	{
	public:
		void update() override;
		void drawComponent() override;
		const char* getText();
	protected:
		std::string text;
	};

	/*TODO
	class ListButton : public GUIcomponent
	{
	public:
		void update() override;
		void drawComponent() override;
		virtual std::string getText();
	private:
		std::string text;
	};
	*/

	struct sliderHandle
	{
		Vector2 center{ 0, 0 };
		float radius = 0;
		Texture2D* texture;
	};

	class Slider : public GUIcomponent
	{
	public:
		void update() override;
		void drawComponent() override;
		const char* getText();
		sliderHandle getHandle();
	private:
		sliderHandle handle;
		std::string text;
	};

	void drawTextWithSecondFont(const char* text, float posX, float posY, float fontSize, Color color);

}

#endif UI_H
