#include "ui.h"

#include "Scenes/scene_manager.h"
#include "ScreenConfig/screen_config.h"
#include "Input/input.h"
#include "EventSystem/event_system.h"
#include "Draw/textures.h"

namespace ui
{

	using namespace textures;

	const float BUTTON_BASE_WIDTH = 1 / 4.f;
	const float BUTTON_BASE_HEIGHT = 1 / 10.f;
	const float SLIDER_BASE_WIDTH = 1 / 4.f;
	const float SLIDER_BASE_HEIGHT = 1 / 40.f;

	const Vector2 SLIDER_HANDLE_SIZE = { 0.030f ,  0.038f };
	const int SLIDER_MAX_HANDLE_POSITIONS = 10;

	const int maxSizeTextButton = 25;

	void GUIcomponent::setRectangle(Rectangle rec)
	{
		collider = rec;
	}
	Rectangle GUIcomponent::getRectangle()
	{
		return collider;
	}

	const char* Button::getText()
	{
		return text.c_str();
	}
	void Button::drawComponent()
	{
		if(active)
		{
			Texture2D* buttonTexture = nullptr;
			if (selected)
			{
				buttonTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::BUTTON_SELECTED);
			}
			else
			{
				buttonTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::BUTTON_NORMAL);

			}
			DrawTexturePro(*buttonTexture, { 0,0, static_cast<float>(buttonTexture->width), static_cast<float>(buttonTexture->height) }, getRectangle(), { 0,0 }, 0, WHITE);
			
			drawTextWithSecondFont(getText(), getRectangle().x + getRectangle().width / 2,
				getRectangle().y + getRectangle().height / 2, screenConfig::textScreenModifier(TEXT_SIZE_SCALE), BLACK);
		}
	}
	void Button::update()
	{
		if (active)
		{
			if (CheckCollisionPointRec(input::currentInput.currentMousePosition, getRectangle()))
			{
				if(!selected)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::MOVE_MENU);
				}
				selected = true;
				if (input::currentInput.leftClick_pressed)
				{
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::SELECT_MENU);
					action();
				}
			}
			else
			{
				selected = false;
			}
		}
	}


	void Slider::update()
	{
		if(active)
		{
			if (CheckCollisionPointRec(input::currentInput.currentMousePosition, getRectangle())
				|| CheckCollisionPointCircle(input::currentInput.currentMousePosition, getHandle().center, getHandle().radius))
			{

				selected = true;
				if (input::currentInput.leftClick_down)
				{

					action();
				}
			}
			else
			{
				selected = false;
			}
		}
	}
	void Slider::drawComponent()
	{
		if(active)
		{
			if (selected)
			{
				DrawTexture(*textures::getUiTexture(textures::UI_TEXTURES_ID::SLIDERBAR_NORMAL), static_cast<int>(getRectangle().x), static_cast<int>(getRectangle().y), WHITE);
				DrawTexture(*textures::getUiTexture(textures::UI_TEXTURES_ID::SLIDERHANDLE_NORMAL), static_cast<int>(getHandle().center.x - getHandle().radius), static_cast<int>(getHandle().center.y - getHandle().radius), WHITE);
			}
			else
			{
				DrawTexture(*textures::getUiTexture(textures::UI_TEXTURES_ID::SLIDERBAR_SELECTED), static_cast<int>(getRectangle().x), static_cast<int>(getRectangle().y), WHITE);
				DrawTexture(*textures::getUiTexture(textures::UI_TEXTURES_ID::SLIDERHANDLE_SELECTED), static_cast<int>(getHandle().center.x - getHandle().radius), static_cast<int>(getHandle().center.y - getHandle().radius), WHITE);
			}
		}
	}
	sliderHandle Slider::getHandle()
	{
		return handle;
	}
	const char* Slider::getText()
	{
		const char* cText = text.c_str();
		return cText;
	}

	void mouseHoverClickLogic(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			if(GUI_Ptr[i] != nullptr)
			{
				GUI_Ptr[i]->update();
			}
			if(sceneConfig::sceneChange)
			{
				sceneConfig::sceneChange = false;
				return;
			}
		}
	}

	void drawAllComponents(const int GUI_SIZE, GUIcomponent* GUI_Ptr[])
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			if (GUI_Ptr[i] != nullptr)
			{
				GUI_Ptr[i]->drawComponent();
			}
		}
	}

	void drawTextWithSecondFont(const char* text, float posX, float posY, float fontSize, Color color)
	{
		float auxPosX = posX - MeasureTextEx(*getFont(FONTS_ID::BASE_FONT), text, fontSize, 1).x / 2;
		float auxPosY = posY - MeasureTextEx(*getFont(FONTS_ID::BASE_FONT), text, fontSize, 1).y / 2;
		DrawTextEx(*getFont(FONTS_ID::BASE_FONT), text, { auxPosX, auxPosY }, fontSize, 1, color);
	}

}