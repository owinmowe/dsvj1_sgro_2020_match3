#include "health_bar.h"
#include "ScreenConfig/screen_config.h"

namespace ui
{
	namespace in_game
	{

		const Rectangle BAR_OFFSET = { 0.005f, 0.005f , -0.01f, -0.01f };
		const Rectangle ELEMENT_OFFSET = { 0.105f, -0.01f, -0.07f, 0.02f };

		HealthBar::HealthBar()
		{
			overlayRectangle = { 0,0,0,0 };
			barRectangle = { 0,0,0,0 };
			elementRectangle = { 0,0,0,0 };
			maxValueWidth = 0;
			overlayTexture = nullptr;
			barTexture = nullptr;
			currentElementTexture = nullptr;
		}
		void HealthBar::draw()
		{
			DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, overlayRectangle, { 0,0 }, 0, WHITE);
			DrawTexturePro(*barTexture, { 0, 0, static_cast<float>(barTexture->width), static_cast<float>(barTexture->height) }, barRectangle, { 0,0 }, 0, WHITE);
			DrawTexturePro(*currentElementTexture, { 0, 0, static_cast<float>(currentElementTexture->width), static_cast<float>(currentElementTexture->height) }, elementRectangle, { 0,0 }, 0, WHITE);
		}
		void HealthBar::setRectangle(Rectangle rec)
		{
			overlayRectangle = rec;
			barRectangle.x = overlayRectangle.x + screenConfig::screenModifier(BAR_OFFSET).x;
			barRectangle.y = overlayRectangle.y + screenConfig::screenModifier(BAR_OFFSET).y;
			barRectangle.width = overlayRectangle.width + screenConfig::screenModifier(BAR_OFFSET).width;
			barRectangle.height = overlayRectangle.height + screenConfig::screenModifier(BAR_OFFSET).height;
			elementRectangle.x = overlayRectangle.x + screenConfig::screenModifier(ELEMENT_OFFSET).x;
			elementRectangle.y = overlayRectangle.y + screenConfig::screenModifier(ELEMENT_OFFSET).y;
			elementRectangle.width = overlayRectangle.width + screenConfig::screenModifier(ELEMENT_OFFSET).width;
			elementRectangle.height = overlayRectangle.height + screenConfig::screenModifier(ELEMENT_OFFSET).height;
			maxValueWidth = barRectangle.width;
		}
		Rectangle HealthBar::getRectangle()
		{
			return overlayRectangle;
		}
		void HealthBar::setOverlayTexture(Texture2D* tex)
		{
			overlayTexture = tex;
		}
		Texture2D* HealthBar::getOverlayTexture()
		{
			return overlayTexture;
		}
		void HealthBar::setBarTexture(Texture2D* tex)
		{
			barTexture = tex;
		}
		Texture2D* HealthBar::getBarTexture()
		{
			return barTexture;
		}
		void HealthBar::setCurrentElementTexture(Texture2D* tex)
		{
			currentElementTexture = tex;
		}
		Texture2D* HealthBar::getCurrentElementTexture()
		{
			return currentElementTexture;
		}
		void HealthBar::updateBar(float currentLife, float maxLife)
		{
			if(currentLife > 0)
			{
				barRectangle.width = (maxValueWidth * currentLife) / maxLife;
			}
			else
			{
				barRectangle.width = 0;
			}
		}
	}
}

