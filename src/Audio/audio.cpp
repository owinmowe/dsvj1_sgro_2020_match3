#include "audio.h"
#include "raylib.h"
#include <vector>
#include <string>
#include "EventSystem/event_system.h"
#include "Scenes/scene_manager.h"

namespace audio
{

    float musicVolume = 0.3f;
    float soundVolume = 0.1f;
    const int MAX_SIZE_FILEPATH = 100;

    struct SoundFile
    {
        Sound sound;
        char FILE_PATH[MAX_SIZE_FILEPATH];
    };

    struct MusicFile
    {
        Music music;
        char FILE_PATH[MAX_SIZE_FILEPATH];
    };

    std::vector<MusicFile> musicFiles;
    enum class MUSIC_ID{MENUMUSIC_ID, GAMEMUSIC_ID, FINAL_BOSS_MUSIC_ID};

    std::vector<SoundFile> soundFiles;
    enum class SOUND_ID{ MENU_MOVE_ID, MENU_SELECT_ID,
    ELECTRIC_COMBO_ID, WIND_COMBO_ID, FIRE_COMBO_ID, WATER_COMBO_ID, ICE_COMBO_ID, EARTH_COMBO_ID,
    ELECTRIC_HIT_ID, WIND_HIT_ID, FIRE_HIT_ID, WATER_HIT_ID, ICE_HIT_ID, EARTH_HIT_ID,
    PLAYER_HIT_ID, ENEMY_HIT_ID, WON_LEVEL_ID, LOST_LEVEL_ID};

    void setMusicVector()
    {
        musicFiles.push_back({ Music(), "res/assets/audio/music/MenuMusic.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/music/GameMusic.mp3" });
        musicFiles.push_back({ Music(), "res/assets/audio/music/FinalBossMusic.mp3" });
    }

    void setSoundVector()
    {
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/MenuMove.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/MenuSelect.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/ElectricCombo.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/WindCombo.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/FireCombo.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/WaterCombo.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/IceCombo.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/EarthCombo.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/ElectricHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/WindHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/FireHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/WaterHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/IceHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/EarthHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/PlayerHit.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/WonLevel.wav" });
        soundFiles.push_back({ Sound(), "res/assets/audio/sounds/LostLevel.wav" });
    }

    void setAllSoundVolume()
    {
        for (SoundFile& file : soundFiles)
        {
            SetSoundVolume(file.sound, soundVolume);
        }
    }

    void setAllMusicVolume()
    {
        for (MusicFile& file : musicFiles)
        {
            SetMusicVolume(file.music, musicVolume);
        }
    }

    void loadSoundsInMemory()
    {
        setSoundVector();
        for(SoundFile& file : soundFiles)
        {
            file.sound = LoadSound(file.FILE_PATH);
        }
    }

    void unloadSoundsFromMemory()
    {
        for(SoundFile& file : soundFiles)
        {
            UnloadSound(file.sound);
        }
        soundFiles.clear();
    }

    void loadMusicInMemory()
    {
        setMusicVector();
        for(MusicFile& file : musicFiles)
        {
            file.music = LoadMusicStream(file.FILE_PATH);
        }
    }

    void unloadMusicFromMemory()
    {
        for(MusicFile& file : musicFiles)
        {
            UnloadMusicStream(file.music);
        }
        musicFiles.clear();
    }

    void menuAudioUpdate()
    {
        if (sceneConfig::musicOn)
        {
            UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
        }
        if (sceneConfig::sfxOn)
        {
            if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::MOVE_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MENU_MOVE_ID)].sound); }
            if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::SELECT_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MENU_SELECT_ID)].sound); }
        }
    }
    void menuAudioStart()
    {
        StopMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
    }

    void gameAudioUpdate()
    {
        using namespace event_system;
        if (sceneConfig::musicOn)
        {
            UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
        }
        if (sceneConfig::sfxOn)
        {
            if (externalEventSystem->checkEvent(EVENT_ID::MOVE_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MENU_MOVE_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::SELECT_MENU)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::MENU_SELECT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ELECTRIC_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ELECTRIC_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WIND_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WIND_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::FIRE_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::FIRE_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WATER_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WATER_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ICE_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ICE_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::EARTH_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::EARTH_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ELECTRIC_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ELECTRIC_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WIND_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WIND_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::FIRE_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::FIRE_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WATER_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WATER_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ICE_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ICE_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::EARTH_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::EARTH_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::PLAYER_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::PLAYER_HIT_ID)].sound); }
            //if (externalEventSystem->checkEvent(EVENT_ID::WON_LEVEL)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WON_LEVEL_ID)].sound); }
            //if (externalEventSystem->checkEvent(EVENT_ID::LOST_LEVEL)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::LOST_LEVEL_ID)].sound); }
        }
    }
    void gameAudioStart()
    {
        StopMusicStream(musicFiles[static_cast<int>(MUSIC_ID::MENUMUSIC_ID)].music);
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
    }
    void finalBossAudioUpdate()
    {
        using namespace event_system;
        if (sceneConfig::musicOn)
        {
            UpdateMusicStream(musicFiles[static_cast<int>(MUSIC_ID::FINAL_BOSS_MUSIC_ID)].music);
        }
        if (sceneConfig::sfxOn)
        {
            if (externalEventSystem->checkEvent(EVENT_ID::ELECTRIC_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ELECTRIC_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WIND_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WIND_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::FIRE_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::FIRE_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WATER_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WATER_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ICE_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ICE_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::EARTH_COMBO)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::EARTH_COMBO_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ELECTRIC_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ELECTRIC_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WIND_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WIND_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::FIRE_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::FIRE_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::WATER_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WATER_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::ICE_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::ICE_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::EARTH_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::EARTH_HIT_ID)].sound); }
            if (externalEventSystem->checkEvent(EVENT_ID::PLAYER_HIT)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::PLAYER_HIT_ID)].sound); }
            //if (externalEventSystem->checkEvent(EVENT_ID::WON_LEVEL)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::WON_LEVEL_ID)].sound); }
            //if (externalEventSystem->checkEvent(EVENT_ID::LOST_LEVEL)) { PlaySoundMulti(soundFiles[static_cast<int>(SOUND_ID::LOST_LEVEL_ID)].sound); }
        }
    }
    void finalBossAudioStart()
    {
        StopMusicStream(musicFiles[static_cast<int>(MUSIC_ID::GAMEMUSIC_ID)].music);
        PlayMusicStream(musicFiles[static_cast<int>(MUSIC_ID::FINAL_BOSS_MUSIC_ID)].music);
    }
}