#ifndef CREDITS_H
#define CREDITS_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Audio/audio.h"
#include "Draw/textures.h"
#include "Ui/ui.h"
#include "ScreenConfig/screen_config.h"

namespace credits
{

	using namespace ui;

	const char TITLE_TEXT[] = "Credits";
	const float TITLE_SIZE = 1 / 9.f;
	const float TITLE_POSITION = 1 / 15.f;
	const float TEXT_STARTING_POSITION = 1 / 7.f;
	const float TEXT_SEPARATION = 1 / 20.f;
	const float TEXT_SIZE = 1 / 50.f;
	const char CREDITS_TEXT_1[] = "Made by Adrian Sgro with Raylib library raylib.com";
	const char CREDITS_TEXT_2[] = "Font: Liong Regular dafont.com";
	const char CREDITS_TEXT_3[] = "Textures: Textures legally licenced from 'Craftpix' craftpix.net";
	const char CREDITS_TEXT_4[] = "SFX: 'Demo Ancient Magic Pack FREE' of unity store assetstore.unity.com";
	const char CREDITS_TEXT_5[] = "Menu Music: 'Chill Factor' by Purple Planet Music purple-planet.com";
	const char CREDITS_TEXT_6[] = "Game Music: 'Thinking Ahead' by Purple Planet Music purple-planet.com";
	const char CREDITS_TEXT_7[] = "Final Boss Music: 'Emotional Epic Trailer' by Purple Planet Music purple-planet.com";
	const Rectangle OVERLAY_REC = { .1f, 0.2f, .8f, .65f };

	const int GUI_SIZE = 1;
	const Rectangle BUTTON_BACK_REC = { 0.3625f, 0.875f, .275f, .1f };

	class Credits : public SceneBase
	{

	private:
		void init() override;
		GUIcomponent* GUIComponent[GUI_SIZE];
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;

	public:
		Credits();
		~Credits();
		void deInit() override;
		void update() override;
		void draw() override;

	};

	class BackButton : public Button
	{
	public:
		BackButton();
		bool selected = false;
		void action() override;

	private:
	};

}

#endif