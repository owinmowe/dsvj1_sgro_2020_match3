#include "credits.h"

namespace credits
{
	Credits::Credits()
	{
		init();
	}
	Credits::~Credits()
	{
		deInit();
	}
	void Credits::init()
	{
		overlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
		backgroundTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::GENERAL_BACKGROUND);
		backgroundTexture->width = screenConfig::currentScreenWidth;
		backgroundTexture->height = screenConfig::currentScreenHeight;
		GUIComponent[0] = new BackButton;
		GUIComponent[0]->setRectangle(screenConfig::screenModifier(BUTTON_BACK_REC));
		GUIComponent[0]->active = true;
	}
	void Credits::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUIComponent[i];
		}
	}
	void Credits::update()
	{
		mouseHoverClickLogic(GUI_SIZE, GUIComponent);
		audio::menuAudioUpdate();
	}
	void Credits::draw()
	{
		DrawTexture(*backgroundTexture, 0, 0, WHITE);
		DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC), { 0, 0 }, 0, WHITE);
		drawTextWithSecondFont(TITLE_TEXT, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TITLE_POSITION), screenConfig::textScreenModifier(TITLE_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_1, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_2, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION + TEXT_SEPARATION), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_3, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION + TEXT_SEPARATION * 2), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_4, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION + TEXT_SEPARATION * 3), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_5, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION + TEXT_SEPARATION * 4), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_6, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION + TEXT_SEPARATION * 5), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawTextWithSecondFont(CREDITS_TEXT_7, screenConfig::textScreenModifier(.5f), screenConfig::textScreenModifier(TEXT_STARTING_POSITION + TEXT_SEPARATION * 6), screenConfig::textScreenModifier(TEXT_SIZE), WHITE);
		drawAllComponents(GUI_SIZE, GUIComponent);
	}

	BackButton::BackButton()
	{
		text = "Back";
	}
	void BackButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}
}