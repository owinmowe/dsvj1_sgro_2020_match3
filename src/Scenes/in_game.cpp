#include "in_game.h"

namespace in_game
{

	using namespace game_objects::grid;

	InGame::InGame()
	{
		init();
	}
	InGame::~InGame() 
	{
		deInit();
	}
	void InGame::init()
	{
		audio::gameAudioStart();
		textures::LoadGameTextures();
		overlayTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::OVERLAY);
		backgroundTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::GENERAL_BACKGROUND);
		backgroundTexture->width = screenConfig::currentScreenWidth;
		backgroundTexture->height = screenConfig::currentScreenHeight;
		weaknessChartTexture = textures::getUiTexture(textures::UI_TEXTURES_ID::WEAKNESS_CHART);
		grid = new Grid();
		battleBG = new background::BattleBG;
		muteMusicButton = new MuteMusicButton;
		muteMusicButton->setRectangle(screenConfig::screenModifier(MUTE_MUSIC_REC));
		muteMusicButton->active = true;
		muteSFXButton = new MuteSFXButton;
		muteSFXButton->setRectangle(screenConfig::screenModifier(MUTE_SFX_REC));
		muteSFXButton->active = true;
		nextLevelButton = new NextLevelButton;
		nextLevelButton->setRectangle(screenConfig::screenModifier(BUTTON_NEXT_LEVEL_REC));
		pauseButton = new PauseButton;
		pauseButton->setRectangle(screenConfig::screenModifier(BUTTON_PAUSE_REC));
		pauseButton->active = true;
		backButton = new BackButton;
		backButton->setRectangle(screenConfig::screenModifier(BUTTON_BACK_REC));
		backButton->active = true;
		currentLevel = 1;
		battleBG->setCurrentLevel(currentLevel);
		gameEnded = false;
	}
	void InGame::deInit()
	{
		textures::UnloadGameTextures();
		delete grid;
		delete nextLevelButton;
		delete pauseButton;
		delete backButton;
		grid = nullptr;
		nextLevelButton = nullptr;
		pauseButton = nullptr;
		backButton = nullptr;
	}
	void InGame::update()
	{
		if(!sceneConfig::pause)
		{
			grid->update();
			battleBG->update();
			if (!lockGrid)
			{
				gridInput();
			}
		}
		if (lockGrid && !sceneConfig::sceneChange)
		{
			nextLevelButton->update();
			if (dynamic_cast<NextLevelButton*>(nextLevelButton)->nextLevel)
			{
				dynamic_cast<NextLevelButton*>(nextLevelButton)->nextLevel = false;
				loadNextLevel();
			}
		}
		muteMusicButton->update();
		muteSFXButton->update();
		pauseButton->update();
		backButton->update();
		gridEventsUpdate();
		battleBGEventsUpdate();
		if(currentLevel < MAX_CURRENT_LEVEL)
		{
			audio::gameAudioUpdate();
		}
		else
		{
			audio::finalBossAudioUpdate();
		}
	}
	void InGame::draw()
	{
		if(gameEnded)
		{
			backButton->drawComponent();
			grid->draw();
			battleBG->draw();
		}
		if(gameLost)
		{
			backButton->drawComponent();
			grid->draw();
			battleBG->draw();
		}
		else
		{
			DrawTexture(*backgroundTexture, 0, 0, WHITE);
			grid->draw();
			battleBG->draw();
			if (nextLevelButton->active)
			{
				nextLevelButton->drawComponent();
			}
			else
			{
				DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC_2), { 0, 0 }, 0, WHITE);
				DrawTexturePro(*weaknessChartTexture, { 0, 0, static_cast<float>(weaknessChartTexture->width), static_cast<float>(weaknessChartTexture->height) }, screenConfig::screenModifier(WEAKNESS_CHART_REC), { 0, 0 }, 0, WHITE);
				DrawTexturePro(*overlayTexture, { 0, 0, static_cast<float>(overlayTexture->width), static_cast<float>(overlayTexture->height) }, screenConfig::screenModifier(OVERLAY_REC), { 0, 0 }, 0, WHITE);
				muteMusicButton->drawComponent();
				muteSFXButton->drawComponent();
				pauseButton->drawComponent();
				backButton->drawComponent();
			}
		}
	}
	void InGame::battleBGEventsUpdate()
	{
		if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::WON_LEVEL))
		{
			if(currentLevel < MAX_CURRENT_LEVEL)
			{
				nextLevelButton->active = true;
				grid->setLevelComplete();
				lockGrid = true;
			}
			else
			{
				grid->setGameComplete();
				lockGrid = true;
			}
		}
		else if(event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::LOST_LEVEL))
		{
			lockGrid = true;
			grid->setGameOver();
		}
	}
	void InGame::gridEventsUpdate()
	{
		if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::GRID_EVENT))
		{
			if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::ELECTRIC_COMBO))
			{
				battleBG->playerCastSpell(MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL, grid->getComboSize());
			}
			else if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::WIND_COMBO))
			{
				battleBG->playerCastSpell(MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL, grid->getComboSize());
			}
			else if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::FIRE_COMBO))
			{
				battleBG->playerCastSpell(MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL, grid->getComboSize());
			}
			else if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::WATER_COMBO))
			{
				battleBG->playerCastSpell(MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL, grid->getComboSize());
			}
			else if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::ICE_COMBO))
			{
				battleBG->playerCastSpell(MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL, grid->getComboSize());
			}
			else if (event_system::externalEventSystem->checkEvent(event_system::EVENT_ID::EARTH_COMBO))
			{
				battleBG->playerCastSpell(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL, grid->getComboSize());
			}
		}
	}
	void InGame::gridInput()
	{
		if (input::currentInput.leftClick_pressed)
		{
			grid->spellsSelectLogic(input::currentInput.currentMousePosition);
		}
		else if (input::currentInput.leftClick_down)
		{
			grid->spellsDragLogic(input::currentInput.currentMousePosition);
		}
		else if (input::currentInput.leftClick_released)
		{
			grid->spellComboLogic();
		}
		else if (input::currentInput.rightClick_pressed)
		{
			grid->rollAllSpells();
		}
	}
	void InGame::loadNextLevel()
	{
		currentLevel++;
		if (currentLevel == MAX_CURRENT_LEVEL)
		{
			audio::finalBossAudioStart();
		}
		else if (currentLevel > MAX_CURRENT_LEVEL)
		{
			gameEnded = true;
		}
		battleBG->setCurrentLevel(currentLevel);
		grid->reRollSpellsLogic();
		nextLevelButton->active = false;
		lockGrid = false;
	}

	MuteMusicButton::MuteMusicButton()
	{
		sceneConfig::musicOn ? text = "Mute Music" : text = "Unmute Music";
	}
	void MuteMusicButton::action()
	{
		sceneConfig::musicOn = !sceneConfig::musicOn;
		sceneConfig::musicOn ? text = "Mute Music" : text = "Unmute Music";
	}
	MuteSFXButton::MuteSFXButton()
	{
		sceneConfig::sfxOn ? text = "Mute SFX" : text = "Unmute SFX";
	}
	void MuteSFXButton::action()
	{
		sceneConfig::sfxOn = !sceneConfig::sfxOn;
		sceneConfig::sfxOn ? text = "Mute SFX" : text = "Unmute SFX";
	}
	NextLevelButton::NextLevelButton()
	{
		text = "Next Level";
	}
	void NextLevelButton::action()
	{
		nextLevel = true;
	}
	PauseButton::PauseButton()
	{
		sceneConfig::pause ? text = "Unpause" : text = "Pause";
	}
	void PauseButton::action()
	{
		sceneConfig::pause = !sceneConfig::pause;
		sceneConfig::pause ? text = "Unpause" : text = "Pause";
	}
	BackButton::BackButton()
	{
		text = "Main Menu";
	}
	void BackButton::action()
	{
		sceneConfig::pause = false;
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}
}