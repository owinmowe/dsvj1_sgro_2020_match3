#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Audio/audio.h"
#include "Ui/ui.h"

namespace main_menu
{

	using namespace ui;

	const char TITLE_TEXT[] = "Wizard Rookie";
	const float TITLE_SIZE = 1 / 9.f;
	const float TITLE_POSITION = 1 / 10.f;
	const Rectangle OVERLAY_REC = { .35f, 0.3f, .3f, .675f };
	const char VERSION_TEXT[] = "v1.05";
	const float VERSION_SIZE = 1 / 50.f;
	const Vector2 VERSION_POSITION = { 7 / 8.f,  15 / 16.f };
	const int GUI_SIZE = 5;
	const float GUI_STARTING_POSITION = 1 / 3.f;
	const float GUI_SEPARATION = 1 / 8.f;
	const float GUI_WIDTH = 1 / 4.f;
	const float GUI_HEIGHT = 1 / 9.f;

	class MainMenu : public SceneBase
	{
	private:
		void init() override;
		GUIcomponent* GUI_Ptr[GUI_SIZE];
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;
	public:
		MainMenu();
		~MainMenu();
		void deInit() override;
		void update() override;
		void draw() override;
	};

	class PlayButton : public Button
	{
	public:
		PlayButton();
		bool selected = false;
		void action() override;
	};
	class RulesButton : public Button
	{
	public:
		RulesButton();
		bool selected = false;
		void action() override;
	};
	class OptionsButton : public Button
	{
	public:
		OptionsButton();
		bool selected = false;
		void action() override;
	};
	class CreditsButton : public Button
	{
	public:
		CreditsButton();
		bool selected = false;
		void action() override;
	};
	class ExitButton : public Button
	{
	public:
		ExitButton();
		bool selected = false;
		void action() override;
	};

}

#endif
