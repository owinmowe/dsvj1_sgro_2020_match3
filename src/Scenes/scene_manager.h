#ifndef SCENES_MANAGER_H
#define SCENES_MANAGER_H
#include "scenes_base.h"
#include "main_menu.h"
#include "credits.h"
#include "options.h"
#include "in_game.h"
#include "basic_rules.h"

namespace sceneConfig
{

	enum class Scene { MAIN_MENU, OPTIONS, CHANGE_KEYS, CREDITS, IN_GAME, BASIC_RULES };
	extern SceneBase* currentScene_Ptr;
	extern bool playing;
	extern bool sceneChange;
	extern bool pause;
	extern bool sfxOn;
	extern bool musicOn;

	void ChangeSceneTo(sceneConfig::Scene nextScene);

}

#endif SCENES_MANAGER_H
