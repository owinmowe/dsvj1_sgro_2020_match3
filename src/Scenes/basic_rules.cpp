#include "basic_rules.h"

namespace basic_rules
{
	BasicRules::BasicRules()
	{
		init();
	}
	BasicRules::~BasicRules()
	{
		deInit();
	}
	void BasicRules::init()
	{
		rulesTextures = textures::getUiTexture(textures::UI_TEXTURES_ID::RULES_BG);
		GUIComponent[0] = new BackButton;
		GUIComponent[0]->setRectangle(screenConfig::screenModifier(BUTTON_BACK_REC));
		GUIComponent[0]->active = true;
	}
	void BasicRules::deInit()
	{
		for (int i = 0; i < GUI_SIZE; i++)
		{
			delete GUIComponent[i];
		}
	}
	void BasicRules::update()
	{
		mouseHoverClickLogic(GUI_SIZE, GUIComponent);
		audio::menuAudioUpdate();
	}
	void BasicRules::draw()
	{
		DrawTexturePro(*rulesTextures, { 0, 0, static_cast<float>(rulesTextures->width), static_cast<float>(rulesTextures->height) }, screenConfig::screenModifier(RULES_BG_REC), { 0, 0 }, 0, WHITE);
		drawAllComponents(GUI_SIZE, GUIComponent);
	}

	BackButton::BackButton()
	{
		text = "Back";
	}
	void BackButton::action()
	{
		sceneConfig::ChangeSceneTo(sceneConfig::Scene::MAIN_MENU);
	}
}