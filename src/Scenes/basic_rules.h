#ifndef BASIC_RULES_H
#define BASIC_RULES_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "Audio/audio.h"
#include "Draw/textures.h"
#include "Ui/ui.h"
#include "ScreenConfig/screen_config.h"

namespace basic_rules
{

	using namespace ui;

	const Rectangle OVERLAY_REC = { .1f, 0.2f, .8f, .65f };
	const Rectangle RULES_BG_REC = { .0f, .0f, 1.f, 1.f };

	const int GUI_SIZE = 1;
	const Rectangle BUTTON_BACK_REC = { 0.3625f, 0.875f, .275f, .1f };

	class BasicRules : public SceneBase
	{

	private:
		void init() override;
		GUIcomponent* GUIComponent[GUI_SIZE];
		Texture2D* rulesTextures = nullptr;

	public:
		BasicRules();
		~BasicRules();
		void deInit() override;
		void update() override;
		void draw() override;

	};

	class BackButton : public Button
	{
	public:
		BackButton();
		bool selected = false;
		void action() override;

	private:
	};

}

#endif
