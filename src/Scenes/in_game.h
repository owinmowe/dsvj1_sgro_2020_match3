
#ifndef IN_GAME_H
#define IN_GAME_H

#include "scenes_base.h"
#include "scene_manager.h"
#include "ScreenConfig/screen_config.h"
#include "Audio/audio.h"
#include "Draw/textures.h"
#include "Input/input.h"
#include "EventSystem/event_system.h"
#include "Ui/ui.h"
#include "gameObjects/grid.h"
#include "gameObjects/battleBG.h"

namespace in_game
{

	const float TIME_TO_RE_ROLL_SPELLS = 2.5f;
	const int MAX_CURRENT_LEVEL = 7;
	const Rectangle BUTTON_NEXT_LEVEL_REC = { 0.4f, 0.2f, .2f, .1f };

	const Rectangle OVERLAY_REC = { 0.7625f, 0.5f, .175f, .425f };
	const Rectangle MUTE_MUSIC_REC = { 0.775f, 0.525f, .15f, .075f };
	const Rectangle MUTE_SFX_REC = { 0.775f, 0.625f, .15f, .075f };
	const Rectangle BUTTON_PAUSE_REC = { 0.775f, 0.725f, .15f, .075f };
	const Rectangle BUTTON_BACK_REC = { 0.775f, 0.825f, .15f, .075f };

	const Rectangle OVERLAY_REC_2 = { 0.03f, 0.45f, .2125f, .525f };
	const Rectangle WEAKNESS_CHART_REC = { 0.05f, 0.5f, .175f, .425f };

	class InGame : public SceneBase
	{
	public:
		InGame();
		~InGame();
		void draw() override;
		void update() override;
		void battleBGEventsUpdate();
		void gridEventsUpdate();
		void loadNextLevel();
		void gridInput();
	private:
		game_objects::grid::Grid* grid;
		Texture2D* backgroundTexture = nullptr;
		Texture2D* overlayTexture = nullptr;
		Texture2D* weaknessChartTexture = nullptr;
		ui::GUIcomponent* nextLevelButton;
		ui::GUIcomponent* muteMusicButton;
		ui::GUIcomponent* muteSFXButton;
		ui::GUIcomponent* pauseButton;
		ui::GUIcomponent* backButton;
		bool lockGrid = false;
		bool gameLost = false;
		bool gameEnded = false;
		background::BattleBG* battleBG;
		int currentLevel = 1;
		void deInit() override;
		void init() override;
	};

	class MuteMusicButton : public ui::Button
	{
	public:
		MuteMusicButton();
	private:
		void action() override;
	};
	class MuteSFXButton : public ui::Button
	{
	public:
		MuteSFXButton();
	private:
		void action() override;
	};
	class NextLevelButton : public ui::Button
	{
	public:
		NextLevelButton();
		bool nextLevel = false;
	private:
		void action() override;
	};
	class PauseButton : public ui::Button
	{
	public:
		PauseButton();
	private:
		void action() override;
	};
	class BackButton : public ui::Button
	{
	public:
		BackButton();
	private:
		void action() override;
	};
}

#endif

