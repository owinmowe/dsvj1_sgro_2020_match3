#include "draw.h"

#include "ScreenConfig/screen_config.h"
#include "Scenes/scene_manager.h"
#include "Draw/textures.h"
#include "Input/input.h"

void frameCounter();

void drawGame()
{
    frameCounter();
    BeginDrawing();
    ClearBackground(BLACK);
    sceneConfig::currentScene_Ptr->draw();
    DrawTexture(*textures::getUiTexture(textures::UI_TEXTURES_ID::CURSOR), static_cast<int>(input::currentInput.currentMousePosition.x), static_cast<int>(input::currentInput.currentMousePosition.y), WHITE);
    EndDrawing();
}

void frameCounter()
{
    screenConfig::currentFrame++;
    if (screenConfig::currentFrame == screenConfig::FRAMES_PER_SECOND)
    {
        screenConfig::currentFrame = 0;
    }
}