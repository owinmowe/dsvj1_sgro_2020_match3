#include "textures.h"
#include "raylib.h"
#include <string>
#include <iostream>

namespace textures
{

	const int FONT_LOAD_SIZE = 225;
	const int MAX_SIZE_FILEPATH = 100;

	struct FontFile
	{
		Font font;
		char FILE_PATH[MAX_SIZE_FILEPATH];
	};

	struct TextureFile
	{
		Texture2D texture;
		char FILE_PATH[MAX_SIZE_FILEPATH];
	};

	std::vector<FontFile> fontFiles;
	std::vector<TextureFile> uiTextureFiles;
	std::vector<TextureFile> backgroundTextureFiles;
	std::vector<TextureFile> gameObjectsTextureFiles;

	void setFontsVector()
	{
		fontFiles.push_back({ Font(), "res/assets/textures/fonts/base_font.otf" });
	}

	void setUiTexturesVector()
	{
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/button_normal.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/button_selected.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/slider_bar_normal.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/slider_bar_selected.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/slider_handle_normal.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/slider_handle_selected.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/cursor.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/health_bar_overlay.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/health_bar_current.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/overlay.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/background.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/weakness_chart.png" });
		uiTextureFiles.push_back({ Texture2D(), "res/assets/textures/ui/rules_bg.png" });
	}

	void setBackgroundTexturesVector()
	{
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background_overlay.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background1.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background2.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background3.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background4.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background5.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background6.png" });
		backgroundTextureFiles.push_back({ Texture2D(), "res/assets/textures/backgrounds/game_background7.png" });
	}

	void setGameObjectsTexturesVector()
	{
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/earth_spell.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/fire_spell.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/ice_spell.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/lighting_spell.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/water_spell.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/wind_spell.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/earth_spell_selected.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/fire_spell_selected.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/ice_spell_selected.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/lighting_spell_selected.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/water_spell_selected.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/wind_spell_selected.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_grid/spells_grid.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/player_earth_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/player_fire_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/player_ice_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/player_electric_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/player_water_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/player_wind_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/enemy_earth_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/enemy_fire_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/enemy_ice_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/enemy_electric_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/enemy_water_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/spells_bg/enemy_wind_spell_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/mage_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/earth_enemy_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/wind_enemy_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/water_enemy_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/ice_enemy_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/fire_enemy_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/electric_enemy_atlas.png" });
		gameObjectsTextureFiles.push_back({ Texture2D(), "res/assets/textures/game_objects/entities/boss_enemy_atlas.png" });
	}

	void LoadUiTextures()
	{
		setFontsVector();
		for (FontFile& file : fontFiles)
		{
			file.font = LoadFontEx(file.FILE_PATH, FONT_LOAD_SIZE, 0, NULL);
		}

		setUiTexturesVector();
		for (TextureFile& file : uiTextureFiles)
		{
			file.texture = LoadTexture(file.FILE_PATH);
		}
	}
	void UnloadUiTextures()
	{
		for(FontFile& file : fontFiles)
		{
			UnloadFont(file.font);
		}
		fontFiles.clear();

		for(TextureFile& file : uiTextureFiles)
		{
			UnloadTexture(file.texture);
		}
		uiTextureFiles.clear();
	}

	void LoadGameTextures()
	{
		setBackgroundTexturesVector();
		for(TextureFile& file : backgroundTextureFiles)
		{
			file.texture = LoadTexture(file.FILE_PATH);
		}

		setGameObjectsTexturesVector();
		for(TextureFile& file : gameObjectsTextureFiles)
		{
			file.texture = LoadTexture(file.FILE_PATH);
		}
	}
	void UnloadGameTextures()
	{
		for (TextureFile& file : backgroundTextureFiles)
		{
			UnloadTexture(file.texture);
		}
		backgroundTextureFiles.clear();

		for (TextureFile& file : gameObjectsTextureFiles)
		{
			UnloadTexture(file.texture);
		}
		gameObjectsTextureFiles.clear();
	}
	Font* getFont(FONTS_ID id)
	{
		return &fontFiles[static_cast<int>(id)].font;
	}
	Texture2D* getUiTexture(UI_TEXTURES_ID id)
	{
		return &uiTextureFiles[static_cast<int>(id)].texture;
	}
	Texture2D* getGameObjectTexture(GAMEOBJECTS_TEXTURES_ID id)
	{
		return &gameObjectsTextureFiles[static_cast<int>(id)].texture;
	}
	Texture2D* getBackgroundTexture(BACKGROUND_TEXTURES_ID id)
	{
		return &backgroundTextureFiles[static_cast<int>(id)].texture;
	}
}