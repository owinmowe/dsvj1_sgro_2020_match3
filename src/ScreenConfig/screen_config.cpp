#include "screen_config.h"

namespace screenConfig
{
    int currentFrame = 0;
    int currentResolutionConfig = 0;
    int currentScreenWidth = Resolutions[0].first;
    int currentScreenHeight = Resolutions[0].second;

    //Modifies size/pos to adjust to screen size being 0 null and 1 fullscreen 
    Vector2 screenModifier(Vector2 size)
    {
        Vector2 newVector2;
        newVector2.x = size.x * currentScreenWidth;
        newVector2.y = size.y * currentScreenHeight;
        return newVector2;
    }

    //Modifies size/rec to adjust to screen size being 0 null and 1 fullscreen 
    Rectangle screenModifier(Rectangle rec)
    {
        Rectangle newRec;
        newRec.x = rec.x * currentScreenWidth;
        newRec.y = rec.y * currentScreenHeight;
        newRec.width = rec.width * currentScreenWidth;
        newRec.height = rec.height * currentScreenHeight;
        return newRec;
    }

    float textScreenModifier(float size)
    {
        return static_cast<float>(size * screenConfig::currentScreenWidth);
    }
    
}