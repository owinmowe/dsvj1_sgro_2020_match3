#ifndef SCREEN_CONFIG_H
#define SCREEN_CONFIG_H

#include "raylib.h"
#include <tuple>

namespace screenConfig
{
    const int RESOLUTIONS_AMMOUNT = 3;
    extern int currentResolutionConfig;
    const std::pair<const int, const int> Resolutions[RESOLUTIONS_AMMOUNT]{ std::make_pair(1920, 1080), std::make_pair(1600, 900), std::make_pair(1280, 720) };
    const int FRAMES_PER_SECOND = 60;
    extern int currentScreenWidth;
    extern int currentScreenHeight;
    extern int currentFrame;
    Vector2 screenModifier(Vector2 size);
    Rectangle screenModifier(Rectangle rec);
    float textScreenModifier(float size);
   
}

#endif
