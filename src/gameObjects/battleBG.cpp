#include "battleBG.h"
#include "ScreenConfig/screen_config.h"
#include "Draw/textures.h"
#include "entities/Entities_configurations.h"
#include "EventSystem/event_system.h"
#include "Ui/ui.h"

#if DEBUG

#include "Input/input.h"
#include <iostream>

#endif // DEBUG


namespace background
{
	using namespace game_objects::entities;
	using namespace game_objects::magic_spell;

	BattleBG::BattleBG()
	{
		currentEnemy = nullptr;
		player = nullptr;
		levelOver = false;
		Texture2D* currentGameBG = nullptr;
		currentGameBGRec = screenConfig::screenModifier(GAME_BG_REC);
		gameBGOverlayTexture = textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_OVERLAY);
		gameBGOverlayTexture->width = static_cast<int>(currentGameBGRec.width);
		gameBGOverlayTexture->height = static_cast<int>(currentGameBGRec.height);
		for (int i = 0; i < MAGIC_SPELLS_MAX_AMMOUNT; i++)
		{
			playerMagicSpellsPool[i] = new game_objects::entities::BackgroundMagicSpell(true);
		}
		for (int i = 0; i < MAGIC_SPELLS_MAX_AMMOUNT; i++)
		{
			enemyMagicSpellsPool[i] = new game_objects::entities::BackgroundMagicSpell(false);
		}
	}
	BattleBG::~BattleBG()
	{

	}
	void BattleBG::setCurrentBGTexture(Texture2D* newBGTexture)
	{
		currentGameBGColor = TRANSPARENT_COLOR;
		currentGameBGTexture = newBGTexture;
		currentGameBGTexture->width = static_cast<int>(currentGameBGRec.width);
		currentGameBGTexture->height = static_cast<int>(currentGameBGRec.height);
	}
	void BattleBG::setCurrentLevel(int level)
	{
		if(player != nullptr)
		{
			delete player;
		}
		if(currentEnemy != nullptr)
		{
			delete currentEnemy;
		}
		player = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_ATLAS), player::atlasSize, player::rectangle, screenConfig::screenModifier(player::healthBarOffset), { 0, 0 }, LEVEL_LOAD_SPEED, player::life);
		player->setAnimation(player::idleFramesAnimations, player::spellCastFramesAnimations, player::hurtFramesAnimations, player::deathFramesAnimations, player::nextFrameSpeed);
		switch (level)
		{
		case 1:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_1));
			player->setPosition(screenConfig::screenModifier(player::background_1_Pos));
			if(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::EARTH_ENEMY_ATLAS) != nullptr)
			{
				currentEnemy = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::EARTH_ENEMY_ATLAS), earthEnemy::atlasSize, earthEnemy::rectangle, screenConfig::screenModifier(earthEnemy::healthBarOffset), screenConfig::screenModifier(earthEnemy::backgroundPos), LEVEL_LOAD_SPEED, earthEnemy::life);
				currentEnemy->setAnimation(earthEnemy::idleFramesAnimations, earthEnemy::spellCastFramesAnimations, earthEnemy::hurtFramesAnimations, earthEnemy::deathFramesAnimations, earthEnemy::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(earthEnemy::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(earthEnemy::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(earthEnemy::groundCastOffset));
				currentEnemy->setAI(earthEnemy::timeForAttack, earthEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
			}
			break;
		case 2:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_2));
			player->setPosition(screenConfig::screenModifier(player::background_2_Pos));
			if (textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WIND_ENEMY_ATLAS) != nullptr)
			{
				currentEnemy = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WIND_ENEMY_ATLAS), windEnemy::atlasSize, windEnemy::rectangle, screenConfig::screenModifier(windEnemy::healthBarOffset), screenConfig::screenModifier(windEnemy::backgroundPos), LEVEL_LOAD_SPEED, windEnemy::life);
				currentEnemy->setAnimation(windEnemy::idleFramesAnimations, windEnemy::spellCastFramesAnimations, windEnemy::hurtFramesAnimations, windEnemy::deathFramesAnimations, windEnemy::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(windEnemy::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(windEnemy::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(windEnemy::groundCastOffset));
				currentEnemy->setAI(windEnemy::timeForAttack, windEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL);
			}
			break;
		case 3:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_3));
			player->setPosition(screenConfig::screenModifier(player::background_3_Pos));
			if (textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WATER_ENEMY_ATLAS) != nullptr)
			{
				currentEnemy = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WATER_ENEMY_ATLAS), waterEnemy::atlasSize, waterEnemy::rectangle, screenConfig::screenModifier(waterEnemy::healthBarOffset), screenConfig::screenModifier(waterEnemy::backgroundPos), LEVEL_LOAD_SPEED, waterEnemy::life);
				currentEnemy->setAnimation(waterEnemy::idleFramesAnimations, waterEnemy::spellCastFramesAnimations, waterEnemy::hurtFramesAnimations, waterEnemy::deathFramesAnimations, waterEnemy::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(waterEnemy::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(waterEnemy::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(waterEnemy::groundCastOffset));
				currentEnemy->setAI(waterEnemy::timeForAttack, waterEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL);
			}
			break;
		case 4:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_4));
			player->setPosition(screenConfig::screenModifier(player::background_4_Pos));
			if (textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ICE_ENEMY_ATLAS) != nullptr)
			{
				currentEnemy = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ICE_ENEMY_ATLAS), iceEnemy::atlasSize, iceEnemy::rectangle, screenConfig::screenModifier(iceEnemy::healthBarOffset), screenConfig::screenModifier(iceEnemy::backgroundPos), LEVEL_LOAD_SPEED, iceEnemy::life);
				currentEnemy->setAnimation(iceEnemy::idleFramesAnimations, iceEnemy::spellCastFramesAnimations, iceEnemy::hurtFramesAnimations, iceEnemy::deathFramesAnimations, iceEnemy::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(iceEnemy::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(iceEnemy::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(iceEnemy::groundCastOffset));
				currentEnemy->setAI(iceEnemy::timeForAttack, iceEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL);
			}
			break;
		case 5:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_5));
			player->setPosition(screenConfig::screenModifier(player::background_5_Pos));
			if (textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::FIRE_ENEMY_ATLAS) != nullptr)
			{
				currentEnemy = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::FIRE_ENEMY_ATLAS), fireEnemy::atlasSize, fireEnemy::rectangle, screenConfig::screenModifier(fireEnemy::healthBarOffset), screenConfig::screenModifier(fireEnemy::backgroundPos), LEVEL_LOAD_SPEED, fireEnemy::life);
				currentEnemy->setAnimation(fireEnemy::idleFramesAnimations, fireEnemy::spellCastFramesAnimations, fireEnemy::hurtFramesAnimations, fireEnemy::deathFramesAnimations, fireEnemy::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(fireEnemy::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(fireEnemy::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(fireEnemy::groundCastOffset));
				currentEnemy->setAI(fireEnemy::timeForAttack, fireEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL);
			}
			break;
		case 6:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_6));
			player->setPosition(screenConfig::screenModifier(player::background_6_Pos));
			if (textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ELECTRIC_ENEMY_ATLAS) != nullptr)
			{
				currentEnemy = new Character(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ELECTRIC_ENEMY_ATLAS), electricEnemy::atlasSize, electricEnemy::rectangle, screenConfig::screenModifier(electricEnemy::healthBarOffset), screenConfig::screenModifier(electricEnemy::backgroundPos), LEVEL_LOAD_SPEED, electricEnemy::life);
				currentEnemy->setAnimation(electricEnemy::idleFramesAnimations, electricEnemy::spellCastFramesAnimations, electricEnemy::hurtFramesAnimations, electricEnemy::deathFramesAnimations, electricEnemy::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(electricEnemy::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(electricEnemy::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(electricEnemy::groundCastOffset));
				currentEnemy->setAI(electricEnemy::timeForAttack, electricEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL);
			}
			break;
		case 7:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_7));
			player->setPosition(screenConfig::screenModifier(player::background_7_Pos));
			if (textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::BOSS_ATLAS) != nullptr)
			{
				currentEnemy = new Boss(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::BOSS_ATLAS), boss::atlasSize, boss::rectangle, screenConfig::screenModifier(boss::healthBarOffset), screenConfig::screenModifier(boss::backgroundPos), LEVEL_LOAD_SPEED, boss::life);
				currentEnemy->setAnimation(boss::idleFramesAnimations, boss::spellCastFramesAnimations, boss::hurtFramesAnimations, boss::deathFramesAnimations, boss::nextFrameSpeed);
				currentEnemy->setColliderOffset(screenConfig::screenModifier(boss::colliderOffset));
				currentEnemy->setGroundCastPosition(screenConfig::screenModifier(boss::airCastOffset));
				currentEnemy->setAirCastPosition(screenConfig::screenModifier(boss::groundCastOffset));
				currentEnemy->setAI(boss::timeForAttack, earthEnemy::attackType);
				currentEnemy->setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
			}
			break;
		default:
			setCurrentBGTexture(textures::getBackgroundTexture(textures::BACKGROUND_TEXTURES_ID::GAME_BACKGROUND_1));
			break;
		}
		currentEnemy->setHealthBarTextures(textures::getUiTexture(textures::UI_TEXTURES_ID::HEALTH_BAR_OVERLAY), textures::getUiTexture(textures::UI_TEXTURES_ID::HEALTH_BAR_CURRENT));
		player->setHealthBarTextures(textures::getUiTexture(textures::UI_TEXTURES_ID::HEALTH_BAR_OVERLAY), textures::getUiTexture(textures::UI_TEXTURES_ID::HEALTH_BAR_CURRENT));
		player->setColliderOffset(screenConfig::screenModifier(player::colliderOffset));
		player->setGroundCastPosition(screenConfig::screenModifier(player::airCastOffset));
		player->setAirCastPosition(screenConfig::screenModifier(player::groundCastOffset));
		levelOver = false;
	}
	void BattleBG::draw()
	{
		DrawTexture(*currentGameBGTexture, static_cast<int>(currentGameBGRec.x), static_cast<int>(currentGameBGRec.y), currentGameBGColor);
		DrawTexture(*gameBGOverlayTexture, static_cast<int>(currentGameBGRec.x), static_cast<int>(currentGameBGRec.y), BLACK);
		if(player != nullptr)
		{
			player->draw();
		}
		if(currentEnemy != nullptr)
		{
			currentEnemy->draw();
		}
		for (int i = 0; i < MAGIC_SPELLS_MAX_AMMOUNT; i++)
		{
			if(playerMagicSpellsPool[i]->isActive())
			{
				playerMagicSpellsPool[i]->draw();
			}
			if (enemyMagicSpellsPool[i]->isActive())
			{
				enemyMagicSpellsPool[i]->draw();
			}
		}
	}
	void BattleBG::update()
	{
		if(currentGameBGColor.a + LEVEL_LOAD_SPEED < 255)
		{
			currentGameBGColor.a += static_cast<unsigned char>(LEVEL_LOAD_SPEED);
		}
		else
		{
			currentGameBGColor.a = static_cast<unsigned char>(255);
		}
		if (player != nullptr)
		{
			player->update();
		}
		if (currentEnemy != nullptr)
		{
			currentEnemy->update();
		}
		for (int i = 0; i < MAGIC_SPELLS_MAX_AMMOUNT; i++)
		{

			if(playerMagicSpellsPool[i]->isActive())
			{
				playerMagicSpellsPool[i]->update();
				if (currentEnemy != nullptr && CheckCollisionRecs(playerMagicSpellsPool[i]->getCollider(), currentEnemy->getCollider()))
				{
					if(playerMagicSpellsPool[i]->getCurrentAnimationState() != SPELL_ANIMATION_STATE::IMPACT)
					{
						playerMagicSpellsPool[i]->impact();
						if (currentEnemy->takeDamage(playerMagicSpellsPool[i]->getSpellType(), static_cast<float>(playerMagicSpellsPool[i]->getDamage())))
						{
							currentEnemy->startDeadAnimation();
							levelOver = true;
							startNextLevel();
						}
						else
						{
							currentEnemy->startHurtAnimation();
						}
					}
				}
			}
			if (enemyMagicSpellsPool[i]->isActive())
			{
				enemyMagicSpellsPool[i]->update();
				if (player != nullptr && CheckCollisionRecs(enemyMagicSpellsPool[i]->getCollider(), player->getCollider()))
				{
					if (enemyMagicSpellsPool[i]->getCurrentAnimationState() != SPELL_ANIMATION_STATE::IMPACT)
					{
						enemyMagicSpellsPool[i]->impact();
						if (player->takeDamage(enemyMagicSpellsPool[i]->getSpellType(), static_cast<float>(enemyMagicSpellsPool[i]->getDamage())))
						{
							player->startDeadAnimation();
							lostLevel();
						}
						else
						{
							player->startHurtAnimation();
						}
					}
				}
			}
		}
		if (currentEnemy->AiAttackNow())
		{
			switch (currentEnemy->getCurrentSpellType())
			{
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
				enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL, windEnemy::spellDamage);
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WIND_COMBO);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
				enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL, waterEnemy::spellDamage);
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WATER_COMBO);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
				enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL, electricEnemy::spellDamage);
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ELECTRIC_COMBO);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
				enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL, iceEnemy::spellDamage);
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ICE_COMBO);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
				enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL, fireEnemy::spellDamage);
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::FIRE_COMBO);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
				enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL, earthEnemy::spellDamage);
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::EARTH_COMBO);
				break;
			default:
				break;
			}
		}
	}
	void BattleBG::lostLevel()
	{
		event_system::externalEventSystem->setEvent(event_system::EVENT_ID::LOST_LEVEL);
	}
	void BattleBG::startNextLevel()
	{
		event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WON_LEVEL);
	}
	void BattleBG::playerCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES type, int damage)
	{
		if (player != nullptr)
		{
			player->startSpellcastAnimation();
			for (int i = 0; i < MAGIC_SPELLS_MAX_AMMOUNT; i++)
			{
				if (!playerMagicSpellsPool[i]->isActive())
				{
					playerMagicSpellsPool[i]->setDamage(damage);
					playerMagicSpellsPool[i]->setSpellType(type);
					player->setCurrentSpellType(type);
					switch (type)
					{
					case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
						playerMagicSpellsPool[i]->activate(windSpell::travelStartFrame, windSpell::hitStartFrame, windSpell::totalFrames, windSpell::nextFrameSpeed, windSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_WIND_SPELL_ATLAS), screenConfig::screenModifier(windSpell::atlasSize));
						playerMagicSpellsPool[i]->setPosition(player->getAirCastPosition());
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(windSpell::colliderOffset));
						break;
					case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
						playerMagicSpellsPool[i]->activate(waterSpell::travelStartFrame, waterSpell::hitStartFrame, waterSpell::totalFrames, waterSpell::nextFrameSpeed, waterSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_WATER_SPELL_ATLAS), screenConfig::screenModifier(waterSpell::atlasSize));
						playerMagicSpellsPool[i]->setPosition(player->getAirCastPosition());
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(waterSpell::colliderOffset));
						break;
					case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
						playerMagicSpellsPool[i]->setPosition(player->getAirCastPosition());
						playerMagicSpellsPool[i]->activate(electricSpell::travelStartFrame, electricSpell::hitStartFrame, electricSpell::totalFrames, electricSpell::nextFrameSpeed, electricSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_LIGHTING_SPELL_ATLAS), screenConfig::screenModifier(electricSpell::atlasSize));
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(electricSpell::colliderOffset));
						break;
					case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
						playerMagicSpellsPool[i]->setPosition(player->getAirCastPosition());
						playerMagicSpellsPool[i]->activate(iceSpell::travelStartFrame, iceSpell::hitStartFrame, iceSpell::totalFrames, iceSpell::nextFrameSpeed, iceSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_ICE_SPELL_ATLAS), screenConfig::screenModifier(iceSpell::atlasSize));
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(iceSpell::colliderOffset));
						break;
					case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
						playerMagicSpellsPool[i]->setPosition(player->getAirCastPosition());
						playerMagicSpellsPool[i]->activate(fireSpell::travelStartFrame, fireSpell::hitStartFrame, fireSpell::totalFrames, fireSpell::nextFrameSpeed, fireSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_FIRE_SPELL_ATLAS), screenConfig::screenModifier(fireSpell::atlasSize));
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(fireSpell::colliderOffset));
						break;
					case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
						playerMagicSpellsPool[i]->setPosition(player->getGroundCastPosition());
						playerMagicSpellsPool[i]->activate(earthSpell::travelStartFrame, earthSpell::hitStartFrame, earthSpell::totalFrames, earthSpell::nextFrameSpeed, earthSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_EARTH_SPELL_ATLAS), screenConfig::screenModifier(earthSpell::atlasSize));
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(earthSpell::colliderOffset));
						break;
					default:
						playerMagicSpellsPool[i]->setSpellType(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
						playerMagicSpellsPool[i]->setPosition(player->getGroundCastPosition());
						playerMagicSpellsPool[i]->activate(earthSpell::travelStartFrame, earthSpell::hitStartFrame, earthSpell::totalFrames, earthSpell::nextFrameSpeed, earthSpell::speed);
						playerMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::PLAYER_BG_EARTH_SPELL_ATLAS), screenConfig::screenModifier(earthSpell::atlasSize));
						playerMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(earthSpell::colliderOffset));
						break;
					}
					break;
				}
			}
		}
	}
	void BattleBG::enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES type, int damage)
	{
		if (!levelOver)
		{
			if (currentEnemy != nullptr)
			{
				currentEnemy->startSpellcastAnimation();
				for (int i = 0; i < MAGIC_SPELLS_MAX_AMMOUNT; i++)
				{
					if (!enemyMagicSpellsPool[i]->isActive())
					{
						enemyMagicSpellsPool[i]->setDamage(damage);
						enemyMagicSpellsPool[i]->setSpellType(type);
						currentEnemy->setCurrentSpellType(type);
						switch (type)
						{
						case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
							enemyMagicSpellsPool[i]->activate(windSpell::travelStartFrame, windSpell::hitStartFrame, windSpell::totalFrames, windSpell::nextFrameSpeed, windSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_WIND_SPELL_ATLAS), screenConfig::screenModifier(windSpell::atlasSize));
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getAirCastPosition());
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(windSpell::colliderOffset));
							break;
						case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
							enemyMagicSpellsPool[i]->activate(waterSpell::travelStartFrame, waterSpell::hitStartFrame, waterSpell::totalFrames, waterSpell::nextFrameSpeed, waterSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_WATER_SPELL_ATLAS), screenConfig::screenModifier(waterSpell::atlasSize));
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getAirCastPosition());
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(waterSpell::colliderOffset));
							break;
						case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getAirCastPosition());
							enemyMagicSpellsPool[i]->activate(electricSpell::travelStartFrame, electricSpell::hitStartFrame, electricSpell::totalFrames, electricSpell::nextFrameSpeed, electricSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_LIGHTING_SPELL_ATLAS), screenConfig::screenModifier(electricSpell::atlasSize));
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(electricSpell::colliderOffset));
							break;
						case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getAirCastPosition());
							enemyMagicSpellsPool[i]->activate(iceSpell::travelStartFrame, iceSpell::hitStartFrame, iceSpell::totalFrames, iceSpell::nextFrameSpeed, iceSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_ICE_SPELL_ATLAS), screenConfig::screenModifier(iceSpell::atlasSize));
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(iceSpell::colliderOffset));
							break;
						case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getAirCastPosition());
							enemyMagicSpellsPool[i]->activate(fireSpell::travelStartFrame, fireSpell::hitStartFrame, fireSpell::totalFrames, fireSpell::nextFrameSpeed, fireSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_FIRE_SPELL_ATLAS), screenConfig::screenModifier(fireSpell::atlasSize));
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(fireSpell::colliderOffset));
							break;
						case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getGroundCastPosition());
							enemyMagicSpellsPool[i]->activate(earthSpell::travelStartFrame, earthSpell::hitStartFrame, earthSpell::totalFrames, earthSpell::nextFrameSpeed, earthSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_EARTH_SPELL_ATLAS), screenConfig::screenModifier(earthSpell::atlasSize));
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(earthSpell::colliderOffset));
							break;
						default:
							enemyMagicSpellsPool[i]->setSpellType(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
							enemyMagicSpellsPool[i]->setPosition(currentEnemy->getGroundCastPosition());
							enemyMagicSpellsPool[i]->activate(earthSpell::travelStartFrame, earthSpell::hitStartFrame, earthSpell::totalFrames, earthSpell::nextFrameSpeed, earthSpell::speed);
							enemyMagicSpellsPool[i]->setTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ENEMY_BG_EARTH_SPELL_ATLAS), screenConfig::screenModifier(earthSpell::atlasSize));
							enemyMagicSpellsPool[i]->setCollider(screenConfig::screenModifier(earthSpell::colliderOffset));
							break;
						}
						break;
					}
				}
			}
		}
	}
}