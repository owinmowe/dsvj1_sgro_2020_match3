#ifndef CHARACTER_H
#define CHARACTER_H

#include "raylib.h"
#include <vector>
#include "gameObjects/spells/magic_spell.h"
#include "ai/ai.h"
#include "Ui/health_bar.h"

namespace game_objects
{
	namespace entities
	{
		using namespace magic_spell;

		const Color TRANSPARENT_COLOR = { 255, 255, 255, 0 };
		const float WEAKNESS_MULTIPLIER = 2.f;
		const float RESISTANCES_MULTIPLIER = .5f;
		const Rectangle HEALTH_BAR_RECTANGLE = { -0.015f, 0.05f, 0.1f, 0.02f };

		const enum class ANIMATION_POS {IDLE, CAST, HIT, DEAD};
		const enum class ANIMATION_STATE {SPAWNING, IDLE, CAST, HIT, DEAD };

		class Character
		{
		public:
			Character(Texture2D* textureAtlas, Vector2 atlasSize, Rectangle rec, Rectangle HealthBarOffset, Vector2 pos, float spawnSpeed, float _life);
			~Character();
			virtual void update();
			void draw();
			void setCharacterTexture(Texture2D* tex);
			void setHealthBarTextures(Texture2D* overlay, Texture2D* bar);
			Texture2D* getCharacterTexture();
			void setAppearSpeed(float alpha);
			void setPosition(Vector2 pos);
			Vector2 getPosition();
			void setRectangle(Rectangle rec);
			Rectangle getRectangle();
			Rectangle getCollider();
			void setColliderOffset(Rectangle colOffset);
			void setAirCastPosition(Vector2 offset);
			void setGroundCastPosition(Vector2 offset);
			Vector2 getAirCastPosition();
			Vector2 getGroundCastPosition();
			void setAnimation(int idleFrames, int spellcastFrames, int hurtFrames, int deathFrames, float nextFrameSpeed);
			void startDeadAnimation();
			void startSpellcastAnimation();
			void startHurtAnimation();
			void setLife(float _life);
			float getLife();
			bool takeDamage(MAGIC_SPELLS_BASIC_TYPES damageType, float damage);
			void setAI(float maxTimeToAttack, MAGIC_SPELLS_BASIC_TYPES spellType);
			MAGIC_SPELLS_BASIC_TYPES getCurrentSpellType();
			void setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES type);
			bool AiAttackNow();
		protected:
			void updateAnimation();
			AIComponent aiComponent;
		private:
			void addResistance(MAGIC_SPELLS_BASIC_TYPES spellType);
			void addWeakness(MAGIC_SPELLS_BASIC_TYPES spellType);
			void clearResistances();
			void clearWeaknesses();
			void updateAI();
			void returnToIdle();
			ui::in_game::HealthBar* healthBar;
			Color currentColor = {255, 255, 255, 0};
			float appearSpeed = 0;
			ANIMATION_STATE currentAnimationState;
			Texture2D* characterAtlas = nullptr;
			Vector2 currentAtlasSize = { 0, 0 };
			Vector2 position = { 0, 0 };
			Rectangle currentRec = { 0, 0, 0, 0 };
			Rectangle collider = { 0, 0, 0, 0 };
			Rectangle customHealthBarOffset = { 0,0,0,0 };
			Vector2 airCastPosition = { 0,0 };
			Vector2 groundCastPosition = { 0,0 };
			MAGIC_SPELLS_BASIC_TYPES currentSpellTypeAttack = MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL;
			float animationSpeed = 0;
			float timeToNextFrame = animationSpeed;
			int idleFramesAnimations = 0;
			int spellCastFramesAnimations = 0;
			int hurtFramesAnimations = 0;
			int deathFramesAnimations = 0;
			float currentLife = 1;
			float maxLife = 1;
			std::vector <MAGIC_SPELLS_BASIC_TYPES> weaknesses;
			std::vector <MAGIC_SPELLS_BASIC_TYPES> resistances;
		};

		class Boss : public Character
		{
		public:
			Boss(Texture2D* textureAtlas, Vector2 atlasSize, Rectangle rec, Rectangle HealthBarOffset, Vector2 pos, float spawnSpeed, float _life);
			void update() override;
		};

	}
}

#endif // !CHARACTER_H

