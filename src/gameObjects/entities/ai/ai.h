#ifndef AI_H
#define AI_H

namespace game_objects
{
	namespace entities
	{
		struct AIComponent
		{
			bool activated = false;
			bool attackNow = false;
			float maxTimeToAttack = 10;
			float timeToAttack = 0;
		};
	}
}



#endif // !AI_H

