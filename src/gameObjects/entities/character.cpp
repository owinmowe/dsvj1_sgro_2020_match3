#include "character.h"
#include "ScreenConfig/screen_config.h"
#include "Draw/textures.h"

namespace game_objects
{
	namespace entities
	{
		Character::Character(Texture2D* textureAtlas, Vector2 atlasSize, Rectangle rec, Rectangle HealthBarOffset, Vector2 pos, float spawnSpeed, float _life)
		{
			healthBar = new ui::in_game::HealthBar();
			currentLife = _life;
			maxLife = currentLife;
			characterAtlas = textureAtlas;
			currentAtlasSize = atlasSize;
			characterAtlas->width = static_cast<int>(screenConfig::screenModifier(currentAtlasSize).x);
			characterAtlas->height = static_cast<int>(screenConfig::screenModifier(currentAtlasSize).y);
			currentRec = screenConfig::screenModifier(rec);
			setPosition(pos);
			currentAnimationState = ANIMATION_STATE::SPAWNING;
			currentColor = { 255, 255, 255, 0 };
			collider = currentRec;
			collider.x = position.x;
			collider.y = position.y;
			airCastPosition = position;
			groundCastPosition = position;
			appearSpeed = spawnSpeed;
			Rectangle aux = { position.x + HealthBarOffset.x, position.y + HealthBarOffset.y, HealthBarOffset.width, HealthBarOffset.height };
			healthBar->setRectangle(aux);
			setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
		}
		Character::~Character()
		{
			delete healthBar;
		}
		void Character::update()
		{
			updateAI();
			updateAnimation();
		}
		void Character::updateAI()
		{
			if (aiComponent.activated)
			{
				aiComponent.attackNow = false;
				aiComponent.timeToAttack -= GetFrameTime();
				if (aiComponent.timeToAttack < 0)
				{
					aiComponent.timeToAttack = aiComponent.maxTimeToAttack;
					aiComponent.attackNow = true;
				}
			}
		}
		void Character::updateAnimation()
		{
			if (currentAnimationState == ANIMATION_STATE::SPAWNING)
			{
				if (currentColor.a + appearSpeed < 255)
				{
					currentColor.a += static_cast<unsigned char>(appearSpeed);
				}
				else
				{
					currentColor.a = 255;
					currentAnimationState = ANIMATION_STATE::IDLE;
				}
			}
			else
			{
				timeToNextFrame -= GetFrameTime();
				if (timeToNextFrame < 0)
				{
					timeToNextFrame = animationSpeed;
					currentRec.x += currentRec.width;
					if (currentAnimationState == ANIMATION_STATE::IDLE)
					{
						if (currentRec.x > currentRec.width* idleFramesAnimations)
						{
							returnToIdle();
						}
					}
					else if (currentAnimationState == ANIMATION_STATE::CAST)
					{
						if (currentRec.x > currentRec.width* spellCastFramesAnimations)
						{
							returnToIdle();
						}
					}
					else if (currentAnimationState == ANIMATION_STATE::HIT)
					{
						if (currentRec.x > currentRec.width* hurtFramesAnimations)
						{
							returnToIdle();
						}
					}
					else  //currentAnimationState == ANIMATION_STATE::DEAD
					{
						if (currentRec.x > currentRec.width* deathFramesAnimations)
						{
							currentRec.x = currentRec.width * deathFramesAnimations;
						}
					}
				}
			}
		}
		void Character::draw()
		{
			DrawTextureRec(*characterAtlas, currentRec, position, currentColor);
			healthBar->draw();
#if DEBUG
			DrawRectangleLinesEx(collider, 5, GREEN);
#endif // DEBUG
		}
		void Character::setCharacterTexture(Texture2D* tex)
		{
			characterAtlas = tex;
		}
		void Character::setHealthBarTextures(Texture2D* overlay, Texture2D* bar)
		{
			healthBar->setBarTexture(bar);
			healthBar->setOverlayTexture(overlay);
		}
		Texture2D* Character::getCharacterTexture()
		{
			return characterAtlas;
		}
		void Character::setAppearSpeed(float alpha)
		{
			appearSpeed = alpha;
		}
		void Character::setPosition(Vector2 pos)
		{
			position = pos;
			healthBar->setRectangle({ healthBar->getRectangle().x + pos.x, healthBar->getRectangle().y + pos.y, healthBar->getRectangle().width, healthBar->getRectangle().height });
		}
		Vector2 Character::getPosition()
		{
			return position;
		}
		void Character::setRectangle(Rectangle rec)
		{
			currentRec = rec;
		}
		Rectangle Character::getRectangle()
		{
			return currentRec;
		}
		Rectangle Character::getCollider()
		{
			return collider;
		}
		void Character::setColliderOffset(Rectangle colOffset)
		{
			collider = currentRec;
			collider.x = position.x;
			collider.y = position.y;
			collider.x += colOffset.x;
			collider.y += colOffset.y;
			collider.width += colOffset.width;
			collider.height += colOffset.height;
		}
		void Character::setAirCastPosition(Vector2 offset)
		{
			groundCastPosition.x = position.x + offset.x;
			groundCastPosition.y = position.y + offset.y;
		}
		void Character::setGroundCastPosition(Vector2 offset)
		{
			airCastPosition.x = position.x + offset.x;
			airCastPosition.y = position.y + offset.y;
		}
		Vector2 Character::getAirCastPosition()
		{
			return airCastPosition;
		}
		Vector2 Character::getGroundCastPosition()
		{
			return groundCastPosition;
		}
		void Character::setAnimation(int idleFrames, int spellcastFrames, int hurtFrames, int deathFrames, float nextFrameSpeed)
		{
			idleFramesAnimations = idleFrames;
			spellCastFramesAnimations = spellcastFrames;
			hurtFramesAnimations = hurtFrames;
			deathFramesAnimations = deathFrames;
			animationSpeed = nextFrameSpeed;
			timeToNextFrame = animationSpeed;
		}
		void Character::setAI(float maxTimeToAttack, MAGIC_SPELLS_BASIC_TYPES spellType)
		{
			aiComponent.maxTimeToAttack = maxTimeToAttack;
			aiComponent.timeToAttack = maxTimeToAttack;
			currentSpellTypeAttack = spellType;
			aiComponent.activated = true;
		}
		MAGIC_SPELLS_BASIC_TYPES Character::getCurrentSpellType()
		{
			return currentSpellTypeAttack;
		}
		void Character::setCurrentSpellType(MAGIC_SPELLS_BASIC_TYPES type)
		{
			resistances.clear();
			weaknesses.clear();
			currentSpellTypeAttack = type;
			switch (type)
			{
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WIND_SPELL));
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WATER_SPELL));
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::LIGHTING_SPELL));
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ICE_SPELL));
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::FIRE_SPELL));
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::EARTH_SPELL));
				break;
			default:
				resistances.push_back(MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL);
				weaknesses.push_back(MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL);
				healthBar->setCurrentElementTexture(textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::EARTH_SPELL));
				break;
			}
		}
		bool Character::AiAttackNow()
		{
			return aiComponent.attackNow;
		}
		void Character::returnToIdle()
		{
			currentRec.x = 0;
			currentRec.y = 0;
			currentAnimationState = ANIMATION_STATE::IDLE;
		}
		void Character::startDeadAnimation()
		{
			if (currentAnimationState != ANIMATION_STATE::DEAD)
			{
				currentRec.x = 0;
				currentRec.y = currentRec.height * static_cast<int>(ANIMATION_POS::DEAD);
				currentAnimationState = ANIMATION_STATE::DEAD;
			}
		}
		void Character::startSpellcastAnimation()
		{
			if (currentAnimationState != ANIMATION_STATE::DEAD)
			{
				currentRec.x = 0;
				currentRec.y = currentRec.height * static_cast<int>(ANIMATION_POS::CAST);
				currentAnimationState = ANIMATION_STATE::CAST;
			}
		}
		void Character::startHurtAnimation()		
		{
			if(currentAnimationState != ANIMATION_STATE::DEAD)
			{
				currentRec.x = 0;
				currentRec.y = currentRec.height * static_cast<int>(ANIMATION_POS::HIT);
				currentAnimationState = ANIMATION_STATE::HIT;
			}
		}
		void Character::setLife(float _life)
		{
			currentLife = _life;
		}
		float Character::getLife()
		{
			return currentLife;
		}
		bool Character::takeDamage(MAGIC_SPELLS_BASIC_TYPES damageType, float damage)
		{
			for (MAGIC_SPELLS_BASIC_TYPES weakness : weaknesses)
			{
				if(weakness == damageType)
				{
					currentLife -= damage * WEAKNESS_MULTIPLIER;
					healthBar->updateBar(currentLife, maxLife);
					return currentLife <= 0;
				}
			}
			for (MAGIC_SPELLS_BASIC_TYPES resistance : resistances)
			{
				if (resistance == damageType)
				{
					currentLife -= damage * RESISTANCES_MULTIPLIER;
					healthBar->updateBar(currentLife, maxLife);
					return currentLife <= 0;
				}
			}
			currentLife -= damage;
			healthBar->updateBar(currentLife, maxLife);
			return currentLife <= 0;
		}
		void Character::addResistance(MAGIC_SPELLS_BASIC_TYPES spellType)
		{
			resistances.push_back(spellType);
		}
		void Character::addWeakness(MAGIC_SPELLS_BASIC_TYPES spellType)
		{
			weaknesses.push_back(spellType);
		}
		void Character::clearResistances()
		{
			resistances.clear();
		}
		void Character::clearWeaknesses()
		{
			weaknesses.clear();
		}

		Boss::Boss(Texture2D* textureAtlas, Vector2 atlasSize, Rectangle rec, Rectangle HealthBarOffset, Vector2 pos, float spawnSpeed, float _life) : Character(textureAtlas, atlasSize, rec, HealthBarOffset, pos, spawnSpeed, _life){}
		void Boss::update()
		{
			if (aiComponent.activated)
			{
				aiComponent.attackNow = false;
				aiComponent.timeToAttack -= GetFrameTime();
				if (aiComponent.timeToAttack < 0)
				{
					aiComponent.timeToAttack = aiComponent.maxTimeToAttack;
					setCurrentSpellType(static_cast<MAGIC_SPELLS_BASIC_TYPES>(GetRandomValue(0, 5)));
					aiComponent.attackNow = true;
				}
			}
			updateAnimation();
		}
	}
}