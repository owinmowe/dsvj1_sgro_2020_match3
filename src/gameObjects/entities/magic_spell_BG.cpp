#include "magic_spell_BG.h"
#include "EventSystem/event_system.h"
#include "ScreenConfig/screen_config.h"

namespace game_objects
{
	namespace entities
	{
		BackgroundMagicSpell::BackgroundMagicSpell(bool fromPlayer)
		{
			texture = nullptr;
			position = { 0, 0,};
			sourceRectangle = { 0, 0, 0, 0 };
			collider = { 0,0,0,0 };
			active = false;
			speed = 0;
			currentAnimationState = SPELL_ANIMATION_STATE::START;
			currentType = magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL;
			currentDamage = 0;
			travelStartFrame = 0;
			hitStartFrame = 0;
			framesNumber = 0;
			timeToNextFrame = 0;
			animationSpeed = 0;
			playerSpell = fromPlayer;
		}
		BackgroundMagicSpell::~BackgroundMagicSpell()
		{

		}
		void BackgroundMagicSpell::update()
		{
			if(currentAnimationState != SPELL_ANIMATION_STATE::IMPACT)
			{
				if(playerSpell)
				{
					position.x += speed * GetFrameTime() * screenConfig::currentScreenWidth;
					collider.x += speed * GetFrameTime() * screenConfig::currentScreenWidth;
				}
				else
				{
					position.x -= speed * GetFrameTime() * screenConfig::currentScreenWidth;
					collider.x -= speed * GetFrameTime() * screenConfig::currentScreenWidth;
				}
			}
			timeToNextFrame -= GetFrameTime();
			if (timeToNextFrame < 0)
			{
				timeToNextFrame = animationSpeed;
				sourceRectangle.x += sourceRectangle.width;
				if (currentAnimationState == SPELL_ANIMATION_STATE::START)
				{
					if (sourceRectangle.x > sourceRectangle.width* travelStartFrame)
					{
						currentAnimationState = SPELL_ANIMATION_STATE::TRAVELING;
					}
				}
				else if (currentAnimationState == SPELL_ANIMATION_STATE::TRAVELING)
				{
					if (sourceRectangle.x > sourceRectangle.width* hitStartFrame)
					{
						sourceRectangle.x = sourceRectangle.width * travelStartFrame;
					}
				}
				else //if(currentAnimationState == SPELL_ANIMATION_STATE::IMPACT)
				{
					if (sourceRectangle.x > texture->width - sourceRectangle.width)
					{
						active = false;
					}
				}
			}
		}
		void BackgroundMagicSpell::draw()
		{
			DrawTextureRec(*texture, sourceRectangle, position, WHITE);
#if DEBUG
			DrawRectangleLinesEx(collider, 5, GREEN);
#endif // DEBUG

		}
		void BackgroundMagicSpell::activate(int travelStart, int hitStart, int totalFrames, float animationSpd, float spd)
		{
			active = true;
			travelStartFrame = travelStart;
			hitStartFrame = hitStart;
			speed = spd;
			framesNumber = totalFrames;
			animationSpeed = animationSpd;
			sourceRectangle.x = 0;
			sourceRectangle.y = 0;
			currentAnimationState = SPELL_ANIMATION_STATE::START;
		}
		void BackgroundMagicSpell::impact()
		{
			currentAnimationState = SPELL_ANIMATION_STATE::IMPACT;
			switch (currentType)
			{
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WIND_HIT);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WATER_HIT);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ELECTRIC_HIT);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ICE_HIT);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::FIRE_HIT);
				break;
			case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::EARTH_HIT);
				break;
			default:
				break;
			}

		}
		SPELL_ANIMATION_STATE BackgroundMagicSpell::getCurrentAnimationState()
		{
			return currentAnimationState;
		}
		void BackgroundMagicSpell::setSpellType(magic_spell::MAGIC_SPELLS_BASIC_TYPES type)
		{
			currentType = type;
		}
		magic_spell::MAGIC_SPELLS_BASIC_TYPES BackgroundMagicSpell::getSpellType()
		{
			return currentType;
		}
		void BackgroundMagicSpell::setDamage(int dmg)
		{
			currentDamage = dmg;
		}
		int BackgroundMagicSpell::getDamage()
		{
			return currentDamage;
		}
		void BackgroundMagicSpell::setTexture(Texture2D* tex, Vector2 atlasSize)
		{
			texture = tex;
			texture->width = static_cast<int>(atlasSize.x);
			texture->height = static_cast<int>(atlasSize.y);
			sourceRectangle.width = static_cast<float>(texture->width / framesNumber);
			sourceRectangle.height = static_cast<float>(texture->height);
		}
		Texture2D* BackgroundMagicSpell::getTexture()
		{
			return texture;
		}
		void BackgroundMagicSpell::setPosition(Vector2 pos)
		{
			position = pos;
		}
		Vector2 BackgroundMagicSpell::getPosition()
		{
			return position;
		}
		void BackgroundMagicSpell::setRectangle(Rectangle rec)
		{
			sourceRectangle = rec;
		}
		Rectangle BackgroundMagicSpell::getRectangle()
		{
			return sourceRectangle;
		}
		Rectangle BackgroundMagicSpell::getCollider()
		{
			return collider;
		}
		void BackgroundMagicSpell::setCollider(Rectangle colliderOffset)
		{
			collider = sourceRectangle;
			collider.x = position.x;
			collider.y = position.y;
			collider.x += colliderOffset.x;
			collider.y += colliderOffset.y;
			collider.width += colliderOffset.width;
			collider.height += colliderOffset.height;
		}
		bool BackgroundMagicSpell::isActive()
		{
			return active;
		}
	}
}