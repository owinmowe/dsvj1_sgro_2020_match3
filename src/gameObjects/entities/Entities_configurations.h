#ifndef ENTITIES_CONFIGURATIONS_H
#define ENTITIES_CONFIGURATIONS_H

#include "raylib.h"
#include "gameObjects/spells/magic_spell.h"

namespace game_objects
{
	namespace entities
	{
		namespace player
		{
			const Vector2 background_1_Pos = { .25f, .2f };
			const Vector2 background_2_Pos = { .25f, .15f };
			const Vector2 background_3_Pos = { .25f, .220f };
			const Vector2 background_4_Pos = { .25f, .210f };
			const Vector2 background_5_Pos = { .25f, .220f };
			const Vector2 background_6_Pos = { .25f, .15f };
			const Vector2 background_7_Pos = { .25f, .15f };
			const Vector2 atlasSize = { 1.4025f, 0.7875f };
			const Rectangle rectangle = { 0, 0, 0.1f, .195f };
			const Rectangle colliderOffset = { .01f, .06f, -0.05f, -0.05f };
			const Rectangle healthBarOffset = { -0.015f, 0.05f, 0.1f, 0.02f };
			const Vector2 airCastOffset = { .0f, -.025f };
			const Vector2 groundCastOffset = { .0f, .01f };
			const float life = 100;
			const float nextFrameSpeed = .075f;
			const int idleFramesAnimations = 0;
			const int spellCastFramesAnimations = 6;
			const int hurtFramesAnimations = 3;
			const int deathFramesAnimations = 9;
		}
		namespace earthEnemy
		{
			const Vector2 backgroundPos = { .525f, .09f };
			const Vector2 atlasSize = { 1.6f, 1.5f };
			const Rectangle rectangle = { 0, 0, .2666f, .375f };
			const Rectangle colliderOffset = { .175f, .1f, -0.15f, -0.15f };
			const Rectangle healthBarOffset = { .12f, 0.1f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { -.1f, .12f };
			const Vector2 airCastOffset = { .0f, .0f };
			const float nextFrameSpeed = .095f;
			const int idleFramesAnimations = 3;
			const int spellCastFramesAnimations = 3;
			const int hurtFramesAnimations = 1;
			const int deathFramesAnimations = 5;
			const float life = 100; //100
			const float timeForAttack = 10;
			const game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES attackType = game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL;
			const int spellDamage = 5;
		}
		namespace windEnemy
		{
			const Vector2 backgroundPos = { .625f, .125f };
			const Vector2 atlasSize = { 0.7f, 0.99f };
			const Rectangle rectangle = { 0, 0, .14f, .2475f };
			const Rectangle colliderOffset = { 0.055f, 0.07f, -0.1f, -0.1f };
			const Rectangle healthBarOffset = { 0.015f, 0.045f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { .0f, .0f };
			const Vector2 airCastOffset = { .0f, .0f };
			const float nextFrameSpeed = .095f;
			const int idleFramesAnimations = 3;
			const int spellCastFramesAnimations = 3;
			const int hurtFramesAnimations = 1;
			const int deathFramesAnimations = 4;
			const float life = 200; //200
			const float timeForAttack = 9.5;
			const game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES attackType = game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL;
			const int spellDamage = 5;
		}
		namespace waterEnemy
		{
			const Vector2 backgroundPos = { .54f, .1525f };
			const Vector2 atlasSize = { 1.8f, 1.5189f };
			const Rectangle rectangle = { 0, 0, .3f, .3795f };
			const Rectangle colliderOffset = { .175f, .1f, -0.225f, -0.225f };
			const Rectangle healthBarOffset = { .12f, 0.125f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { .0f, .0f };
			const Vector2 airCastOffset = { .08f, .09f };
			const float nextFrameSpeed = .125f;
			const int idleFramesAnimations = 2;
			const int spellCastFramesAnimations = 4;
			const int hurtFramesAnimations = 1;
			const int deathFramesAnimations = 5;
			const float life = 300; //300
			const float timeForAttack = 9;
			const game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES attackType = game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL;
			const int spellDamage = 5;
		}
		namespace iceEnemy
		{
			const Vector2 backgroundPos = { .6f, .2f };
			const Vector2 atlasSize = { 0.6948f, 0.988f };
			const Rectangle rectangle = { 0, 0, .13896f, .247f };
			const Rectangle colliderOffset = { .075f, .06f, -0.1f, -0.1f };
			const Rectangle healthBarOffset = { .01f, 0.04f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { .0f, .0f };
			const Vector2 airCastOffset = { -.075f, -.025f };
			const float nextFrameSpeed = .15f;
			const int idleFramesAnimations = 2;
			const int spellCastFramesAnimations = 4;
			const int hurtFramesAnimations = 1;
			const int deathFramesAnimations = 4;
			const float life = 400; //400
			const float timeForAttack = 8.5;
			const game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES attackType = game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL;
			const int spellDamage = 5;
		}
		namespace fireEnemy
		{
			const Vector2 backgroundPos = { .6f, .225f };
			const Vector2 atlasSize = { 0.4166f, 0.9879f };
			const Rectangle rectangle = { 0, 0, .1388f, .2469f };
			const Rectangle colliderOffset = { .06f, .06f, -0.05f, -0.1f };
			const Rectangle healthBarOffset = { .02f, 0.07f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { .0f, .0f };
			const Vector2 airCastOffset = { -.05f, -.03f };
			const float nextFrameSpeed = .25f;
			const int idleFramesAnimations = 2;
			const int spellCastFramesAnimations = 1;
			const int hurtFramesAnimations = 1;
			const int deathFramesAnimations = 1;
			const float life = 500; //500
			const float timeForAttack = 8;
			const game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES attackType = game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL;
			const int spellDamage = 5;
		}
		namespace electricEnemy
		{
			const Vector2 backgroundPos = { .60f, .16f };
			const Vector2 atlasSize = { 0.833f, 0.9879f };
			const Rectangle rectangle = { 0, 0, .1388f, .2469f };
			const Rectangle colliderOffset = { .06f, .06f, -0.1f, -0.1f };
			const Rectangle healthBarOffset = { .02f, 0.06f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { .0f, .0f };
			const Vector2 airCastOffset = { .0f, .0f };
			const float nextFrameSpeed = .075f;
			const int idleFramesAnimations = 4;
			const int spellCastFramesAnimations = 4;
			const int hurtFramesAnimations = 3;
			const int deathFramesAnimations = 5;
			const float life = 600; //600
			const float timeForAttack = 7.5;
			const game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES attackType = game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL;
			const int spellDamage = 5;
		}
		namespace boss
		{
			const Vector2 backgroundPos = { .55f, .14f };
			const Vector2 atlasSize = { 1.f, 1.f };
			const Rectangle rectangle = { 0, 0, 0.2f, 0.25f };
			const Rectangle colliderOffset = { .110f, .065f, -0.05f, -0.05f };
			const Rectangle healthBarOffset = { .08f, 0.04f, 0.1f, 0.02f };
			const Vector2 groundCastOffset = { -.1f, .025f };
			const Vector2 airCastOffset = { .0f, .0f };
			const float nextFrameSpeed = .125f;
			const int idleFramesAnimations = 2;
			const int spellCastFramesAnimations = 4;
			const int hurtFramesAnimations = 1;
			const int deathFramesAnimations = 4;
			const float life = 1000; //1000
			const float timeForAttack = 10;
			const int spellDamage = 5;
		}

		namespace earthSpell
		{
			const Rectangle sourceRectangle = { 0, 0, 0.13845f, 0.2475f };
			const Vector2 atlasSize = { 3.6115f, .2475f };
			const Rectangle colliderOffset = { .05f, .09f, -0.1f, -0.15f };
			const float speed = .2f;
			const int travelStartFrame = 6;
			const int hitStartFrame = 8;
			const int totalFrames = 13;
			const float nextFrameSpeed = .095f;
		}
		namespace windSpell
		{
			const Rectangle sourceRectangle = { 0, 0, 0.13845f, 0.2475f };
			const Vector2 atlasSize = { 1.80555f, .2475f };
			const Rectangle colliderOffset = { .04f, .06f, -0.09f, -0.1f };
			const float speed = .2f;
			const int travelStartFrame = 4;
			const int hitStartFrame = 6;
			const int totalFrames = 11;
			const float nextFrameSpeed = .095f;
		}
		namespace waterSpell
		{
			const Rectangle sourceRectangle = { 0, 0, 0.13845f, 0.2475f };
			const Vector2 atlasSize = { 1.80555f, .2475f };
			const Rectangle colliderOffset = { .04f, .06f, -0.1f, -0.1f };
			const float speed = .2f;
			const int travelStartFrame = 2;
			const int hitStartFrame = 4;
			const int totalFrames = 11;
			const float nextFrameSpeed = .095f;
		}
		namespace iceSpell
		{
			const Rectangle sourceRectangle = { 0, 0, 0.13845f, 0.2475f };
			const Vector2 atlasSize = { 1.80555f, .2475f };
			const Rectangle colliderOffset = { .04f, .06f, -0.06f, -0.1f };
			const float speed = .2f;
			const int travelStartFrame = 2;
			const int hitStartFrame = 4;
			const int totalFrames = 13;
			const float nextFrameSpeed = .095f;
		}
		namespace fireSpell
		{
			const Rectangle sourceRectangle = { 0, 0, 0.13845f, 0.2475f };
			const Vector2 atlasSize = { 1.80555f, .2475f };
			const Rectangle colliderOffset = { .04f, .06f, -0.1f, -0.1f };
			const float speed = .2f;
			const int travelStartFrame = 3;
			const int hitStartFrame = 5;
			const int totalFrames = 10;
			const float nextFrameSpeed = .095f;
		}
		namespace electricSpell
		{
			const Rectangle sourceRectangle = { 0, 0, 0.13845f, 0.2475f };
			const Vector2 atlasSize = { 1.80555f, .2475f };
			const Rectangle colliderOffset = { .04f, .06f, -0.08f, -0.1f };
			const float speed = .2f;
			const int travelStartFrame = 2;
			const int hitStartFrame = 4;
			const int totalFrames = 17;
			const float nextFrameSpeed = .095f;
		}
	}
}

#endif