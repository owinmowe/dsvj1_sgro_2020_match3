#ifndef MAGIC_SPELL_BG_H
#define MAGIC_SPELL_BG_H

#include "raylib.h"
#include "gameObjects/spells/magic_spell.h"

namespace game_objects
{
	namespace entities
	{

		enum class SPELL_ANIMATION_STATE{START, TRAVELING, IMPACT};

		class BackgroundMagicSpell
		{
		public:
			BackgroundMagicSpell(bool fromPlayer);
			~BackgroundMagicSpell();
			void update();
			void draw();
			void activate(int travelStart, int hitStart, int totalFrames, float animationSpd, float spd);
			bool isActive();
			void impact();
			SPELL_ANIMATION_STATE getCurrentAnimationState();
			void setSpellType(magic_spell::MAGIC_SPELLS_BASIC_TYPES type);
			void setDamage(int dmg);
			int getDamage();
			magic_spell::MAGIC_SPELLS_BASIC_TYPES getSpellType();
			void setTexture(Texture2D* tex, Vector2 atlasSize);
			Texture2D* getTexture();
			void setPosition(Vector2 pos);
			Vector2 getPosition();
			void setRectangle(Rectangle rec);
			Rectangle getRectangle();
			void setCollider(Rectangle colliderOffset);
			Rectangle getCollider();
		private:
			Texture2D* texture = nullptr;
			Vector2 position = { 0, 0 };
			Rectangle sourceRectangle = { 0, 0, 0, 0 };
			Rectangle collider = { 0,0,0,0 };
			bool active = false;
			bool playerSpell = true;
			float speed = 0;
			SPELL_ANIMATION_STATE currentAnimationState = SPELL_ANIMATION_STATE::START;
			int currentDamage = 0;
			magic_spell::MAGIC_SPELLS_BASIC_TYPES currentType = magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL;
			int travelStartFrame = 0;
			int hitStartFrame = 0;
			int framesNumber = 0;
			float animationSpeed = 0;
			float timeToNextFrame = animationSpeed;
		};
	}
}



#endif
