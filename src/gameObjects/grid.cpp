#include "grid.h"
#include "raylib.h"
#include "Scenes/scene_manager.h"
#include "EventSystem/event_system.h"
#include "ScreenConfig/screen_config.h"
#include "Draw/textures.h"
#include "Ui/ui.h"

namespace game_objects
{
	namespace grid
	{
		using namespace magic_spell;

		Grid::Grid()
		{
			texture = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::SPELLS_GRID);
			currentRec = screenConfig::screenModifier(GRID_REC);
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = 0; j < GRID_ROWS; j++)
				{
					magicSpellsSlots[i][j] = nullptr;
				}
			}
			texture->width = static_cast<int>(currentRec.width + screenConfig::screenModifier(GRID_PAD_OVERSIZE).x * 2);
			texture->height = static_cast<int>(currentRec.height + screenConfig::screenModifier(GRID_PAD_OVERSIZE).y * 2);
			levelComplete = false;
			rollAllSpells();
		}
		Grid::~Grid()
		{
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = 0; j < GRID_ROWS; j++)
				{
					if (magicSpellsSlots[i][j] != nullptr)
					{
						delete magicSpellsSlots[i][j];
					}
				}
			}
		}
		void Grid::draw()
		{
			DrawTexture(*texture, static_cast<int>(currentRec.x - screenConfig::screenModifier(GRID_PAD_OVERSIZE).x), static_cast<int>(currentRec.y - screenConfig::screenModifier(GRID_PAD_OVERSIZE).y), WHITE);
			if(sceneConfig::pause)
			{
				if (screenConfig::currentFrame / 30 % 2)
				{
					ui::drawTextWithSecondFont(PAUSE_TEXT, screenConfig::screenModifier(PAUSE_TEXT_POSITION).x, screenConfig::screenModifier(PAUSE_TEXT_POSITION).y, screenConfig::textScreenModifier(PAUSE_TEXT_SIZE), BLACK);
				}
			}
			else if(levelComplete)
			{
				ui::drawTextWithSecondFont("Level complete!", screenConfig::screenModifier(TEXT_LEVEL_COMPLETE_POSITION).x,
				screenConfig::screenModifier(TEXT_LEVEL_COMPLETE_POSITION).y, screenConfig::textScreenModifier(TEXT_LEVEL_COMPLETE_SIZE), BLACK);
			}
			else if (gameComplete)
			{
				ui::drawTextWithSecondFont("Game complete!", screenConfig::screenModifier(TEXT_LEVEL_COMPLETE_POSITION).x,
				screenConfig::screenModifier(TEXT_LEVEL_COMPLETE_POSITION).y, screenConfig::textScreenModifier(TEXT_LEVEL_COMPLETE_SIZE), BLACK);
				ui::drawTextWithSecondFont("Thank you for playing!", screenConfig::screenModifier(TEXT_GAME_COMPLETE_POSITION).x,
				screenConfig::screenModifier(TEXT_GAME_COMPLETE_POSITION).y, screenConfig::textScreenModifier(TEXT_GAME_COMPLETE_SIZE), BLACK);
			}
			else if (gameOver)
			{
				ui::drawTextWithSecondFont("Game Over...", screenConfig::screenModifier(TEXT_LEVEL_COMPLETE_POSITION).x,
				screenConfig::screenModifier(TEXT_LEVEL_COMPLETE_POSITION).y, screenConfig::textScreenModifier(TEXT_LEVEL_COMPLETE_SIZE), BLACK);
			}
			else
			{
				for (int i = 0; i < GRID_COLUMNS; i++)
				{
					for (int j = 0; j < GRID_ROWS; j++)
					{
						if (magicSpellsSlots[i][j] != nullptr)
						{
							magicSpellsSlots[i][j]->draw();
						}
					}
				}
			}
		}
		void Grid::update()
		{
			GlobalSelectionAnimationUpdate();
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = 0; j < GRID_ROWS; j++)
				{
					if (magicSpellsSlots[i][j] != nullptr)
					{
						magicSpellsSlots[i][j]->update();
						magicSpellsSlots[i][j]->setSelectedColor(CurrentGlobalSelectedColor);
					}
				}
			}
		}
		void Grid::reRollSpellsLogic()
		{
			rollAllSpells();
		}
		void Grid::spellsSelectLogic(Vector2 mousePos)
		{
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = 0; j < GRID_ROWS; j++)
				{
					if (magicSpellsSlots[i][j] != nullptr)
					{
						if (magicSpellsSlots[i][j]->posOnRec(mousePos))
						{
							firstSpellSelect(i, j);
						}
					}
				}
			}
		}
		void Grid::spellsDragLogic(Vector2 mousePos)
		{
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = 0; j < GRID_ROWS; j++)
				{
					if (magicSpellsSlots[i][j] != nullptr)
					{
						if (magicSpellsSlots[i][j]->posOnRec(mousePos))
						{
							if (selectedPiecesPositions.size() > 0 && magicSpellsSlots[i][j]->getType() == currentSelectedType)
							{
								dragSelectLogic(i, j);
							}
						}
					}
				}
			}
		}
		void Grid::GlobalSelectionAnimationUpdate()
		{
			static bool selectionUp = true;
			if (selectionUp)
			{
				if (CurrentGlobalSelectedColor.a + SELECTION_ANIMATION_SPEED <= 255)
				{
					CurrentGlobalSelectedColor.a += static_cast<unsigned char>(SELECTION_ANIMATION_SPEED);
				}
				else
				{
					CurrentGlobalSelectedColor.a = 255;
					selectionUp = false;
				}
			}
			else
			{
				if (CurrentGlobalSelectedColor.a - SELECTION_ANIMATION_SPEED >= 0)
				{
					CurrentGlobalSelectedColor.a -= static_cast<unsigned char>(SELECTION_ANIMATION_SPEED);
				}
				else
				{
					CurrentGlobalSelectedColor.a = 0;
					selectionUp = true;
				}
			}
		}
		void Grid::firstSpellSelect(int i, int j)
		{
			currentSelectedType = magicSpellsSlots[i][j]->getType();
			selectedPiecesPositions.push_back(magicSpellsSlots[i][j]->getGridPosition());
			magicSpellsSlots[i][j]->setSelectedColor(TRANSPARENT_COLOR);
			CurrentGlobalSelectedColor = TRANSPARENT_COLOR;
			magicSpellsSlots[i][j]->selectToggle();
		}
		void Grid::spellComboLogic()
		{
			if (selectedPiecesPositions.size() >= MIN_SPELLS_FOR_COMBO)
			{
				event_system::externalEventSystem->setEvent(event_system::EVENT_ID::GRID_EVENT);
				comboSize = selectedPiecesPositions.size();
				switch (currentSelectedType)
				{
				case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WIND_COMBO);
					break;
				case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::WATER_COMBO);
					break;
				case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ELECTRIC_COMBO);
					break;
				case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::ICE_COMBO);
					break;
				case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::FIRE_COMBO);
					break;
				case game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
					event_system::externalEventSystem->setEvent(event_system::EVENT_ID::EARTH_COMBO);
					break;
				default:
					break;
				}
				for (Vector2 pos : selectedPiecesPositions)
				{
					magicSpellsSlots[static_cast<int>(pos.x)][static_cast<int>(pos.y)] = nullptr;
				}
				replenishSpellsLogic();
			}
			else
			{
				for (Vector2 pos : selectedPiecesPositions)
				{
					magicSpellsSlots[static_cast<int>(pos.x)][static_cast<int>(pos.y)]->selectToggle();
				}
			}
			selectedPiecesPositions.clear();
		}
		void Grid::replenishSpellsLogic()
		{
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = GRID_ROWS - 1; j >= 0; j--)
				{
					if (magicSpellsSlots[static_cast<int>(i)][static_cast<int>(j)] == nullptr)
					{
						for (int k = j; k >= 0; k--)
						{
							if (magicSpellsSlots[static_cast<int>(i)][static_cast<int>(k)] != nullptr)
							{
								magicSpellsSlots[static_cast<int>(i)][static_cast<int>(k)]->setFallNewPosition(i, j, currentRec);
								MagicSpell* aux = magicSpellsSlots[static_cast<int>(i)][static_cast<int>(j)];
								magicSpellsSlots[static_cast<int>(i)][static_cast<int>(j)] = magicSpellsSlots[static_cast<int>(i)][static_cast<int>(k)];
								magicSpellsSlots[static_cast<int>(i)][static_cast<int>(k)] = aux;
								break;
							}
						}
					}
				}
				for (int j = GRID_ROWS - 1; j >= 0; j--)
				{
					if (magicSpellsSlots[static_cast<int>(i)][static_cast<int>(j)] == nullptr)
					{
						newSingleSpell(i, j);
					}
				}
			}
		}
		void Grid::dragSelectLogic(int i, int j)
		{
			if (selectedPiecesPositions.back().x == i && selectedPiecesPositions.back().y == j)
			{
				return;
			}
			else if (validPiecePosition(i, j))
			{
				if (rollbackSelectionLogic(i, j))
				{
					return;
				}
				if (magicSpellsSlots[i][j]->selectToggle())
				{
					selectedPiecesPositions.push_back(magicSpellsSlots[i][j]->getGridPosition());
				}
			}
			else
			{
				rollbackSelectionLogic(i, j);
			}
		}
		bool Grid::rollbackSelectionLogic(int i, int j)
		{
			for (Vector2 pos : selectedPiecesPositions)
			{
				if (static_cast<int>(pos.x) == i && static_cast<int>(pos.y == j))
				{
					do
					{
						magicSpellsSlots[static_cast<int>(selectedPiecesPositions.back().x)][static_cast<int>(selectedPiecesPositions.back().y)]->setState(MAGIC_SPELL_STATE::LOCKED);
						selectedPiecesPositions.pop_back();
					} while (magicSpellsSlots[i][j]->getGridPosition().x != pos.x && magicSpellsSlots[i][j]->getGridPosition().y != pos.y);
					return true;
				}
			}
			return false;
		}
		bool Grid::validPiecePosition(int i, int j)
		{
			for (int k = -1; k <= 1; k++)
			{
				for (int l = -1; l <= 1; l++)
				{
					if (static_cast<int>(selectedPiecesPositions.back().x) == i + l && static_cast<int>(selectedPiecesPositions.back().y) == j + k)
					{
						return true;
					}
				}
			}
			return false;
		}
		void Grid::rollAllSpells()
		{
			for (int i = 0; i < GRID_COLUMNS; i++)
			{
				for (int j = 0; j < GRID_ROWS; j++)
				{
					newSingleSpell(i, j);
				}
			}
			levelComplete = false;
		}
		void Grid::newSingleSpell(int x, int y)
		{
			if (magicSpellsSlots[x][y] != nullptr)
			{
				delete magicSpellsSlots[x][y];
			}
			MAGIC_SPELLS_BASIC_TYPES randomNewType = static_cast<MAGIC_SPELLS_BASIC_TYPES>(GetRandomValue(0, static_cast<int>(MAGIC_SPELLS_BASIC_TYPES::SIZE) - 1));
			magicSpellsSlots[x][y] = new MagicSpell(randomNewType, x, y, currentRec, GRID_COLUMNS, GRID_ROWS);
		}
		int Grid::getComboSize()
		{
			return comboSize;
		}
		void Grid::setLevelComplete()
		{
			levelComplete = true;
		}
		void Grid::setGameComplete()
		{
			gameComplete = true;
		}
		void Grid::setGameOver()
		{
			gameOver = true;
		}
	}
}