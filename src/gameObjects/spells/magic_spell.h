#ifndef MAGIC_SPELL_H
#define MAGIC_SPELL_H

#include "raylib.h"
#include "Draw/textures.h"

namespace game_objects
{
	namespace magic_spell
	{
		enum class MAGIC_SPELL_STATE { SPAWNING, LOCKED, UNLOCKED, SELECTED, ACTIVATING };

		enum class MAGIC_SPELLS_BASIC_TYPES { WIND_SPELL, WATER_SPELL, LIGHTING_SPELL, ICE_SPELL, FIRE_SPELL, EARTH_SPELL, SIZE };

		const float COLLIDER_OFFSET = .125f;
		const float FALL_SPEED = .25f;
		const float SPAWN_SPEED = 5.f;

		class MagicSpell
		{
		public:
			MagicSpell(MAGIC_SPELLS_BASIC_TYPES type, int posX, int posY, Rectangle newRec, const int GRID_COLUMNS, const int GRID_ROWS);
			void setInstantNewPosition(int posX, int posY, Rectangle& newRec);
			void setFallNewPosition(int posX, int posY, Rectangle& newRec);
			MAGIC_SPELLS_BASIC_TYPES getType();
			void setType(MAGIC_SPELLS_BASIC_TYPES type);
			Vector2 getGridPosition();
			void update();
			void draw();
			MAGIC_SPELL_STATE getState();
			bool posOnRec(Vector2 pos);
			void setState(MAGIC_SPELL_STATE newState);
			void setSelectedColor(Color color);
			bool selectToggle();
		protected:
			void setSpellPositionAndSize(int posX, int posY, Rectangle newRec, const int GRID_COLUMNS, const int GRID_ROWS);
			MAGIC_SPELLS_BASIC_TYPES currentType;
			int gridPositionX = 0;
			int gridPositionY = 0;
			Vector2 currentPosition{ 0,0 };
			Vector2 targetPosition{ 0,0 };
			Rectangle collider{ 0,0,0,0 };
			Texture2D* texture_base = nullptr;
			Texture2D* texture_selected = nullptr;
			MAGIC_SPELL_STATE currentState = MAGIC_SPELL_STATE::SPAWNING;
			Color selectedColor = WHITE;
			Color normalColor = WHITE;
		};
	}

}

#endif
