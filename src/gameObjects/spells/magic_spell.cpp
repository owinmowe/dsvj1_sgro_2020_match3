#include "magic_spell.h"

#include "ScreenConfig/screen_config.h"

namespace game_objects
{

	namespace magic_spell
	{

		MagicSpell::MagicSpell(MAGIC_SPELLS_BASIC_TYPES type, int posX, int posY, Rectangle newRec, const int GRID_COLUMNS, const int GRID_ROWS)
		{
			currentType = type;
			switch (currentType)
			{
			case MAGIC_SPELLS_BASIC_TYPES::WIND_SPELL:
				texture_base = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WIND_SPELL);
				texture_selected = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WIND_SPELL_SELECTED);
				break;
			case MAGIC_SPELLS_BASIC_TYPES::WATER_SPELL:
				texture_base = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WATER_SPELL);
				texture_selected = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::WATER_SPELL_SELECTED);
				break;
			case MAGIC_SPELLS_BASIC_TYPES::LIGHTING_SPELL:
				texture_base = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::LIGHTING_SPELL);
				texture_selected = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::LIGHTING_SPELL_SELECTED);
				break;
			case MAGIC_SPELLS_BASIC_TYPES::ICE_SPELL:
				texture_base = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ICE_SPELL);
				texture_selected = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::ICE_SPELL_SELECTED);
				break;
			case MAGIC_SPELLS_BASIC_TYPES::FIRE_SPELL:
				texture_base = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::FIRE_SPELL);
				texture_selected = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::FIRE_SPELL_SELECTED);
				break;
			case MAGIC_SPELLS_BASIC_TYPES::EARTH_SPELL:
				texture_base = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::EARTH_SPELL);
				texture_selected = textures::getGameObjectTexture(textures::GAMEOBJECTS_TEXTURES_ID::EARTH_SPELL_SELECTED);
				break;
			default:
				break;
			}
			setSpellPositionAndSize(posX, posY, newRec, GRID_COLUMNS, GRID_ROWS);
		}
		void MagicSpell::setSpellPositionAndSize(int posX, int posY, Rectangle newRec, const int GRID_COLUMNS, const int GRID_ROWS)
		{
			texture_base->width = static_cast<int>(newRec.width / GRID_COLUMNS);
			texture_base->height = static_cast<int>(newRec.height / GRID_ROWS);
			texture_selected->width = static_cast<int>(newRec.width / GRID_COLUMNS);
			texture_selected->height = static_cast<int>(newRec.height / GRID_ROWS);
			setInstantNewPosition(posX, posY, newRec);
		}
		void MagicSpell::setInstantNewPosition(int posX, int posY, Rectangle& newRec)
		{
			gridPositionX = posX;
			gridPositionY = posY;
			currentPosition.x = newRec.x + (texture_base->width * gridPositionX);
			currentPosition.y = newRec.y + (texture_base->height * gridPositionY);
			collider.x = currentPosition.x + texture_base->width * COLLIDER_OFFSET;
			collider.y = currentPosition.y + texture_base->height * COLLIDER_OFFSET;
			collider.width = texture_base->width - texture_base->width * COLLIDER_OFFSET * 2;
			collider.height = texture_base->height - texture_base->height * COLLIDER_OFFSET * 2;
			normalColor.a = 0;
			currentState = MAGIC_SPELL_STATE::SPAWNING;
		}
		void MagicSpell::setFallNewPosition(int posX, int posY, Rectangle& newRec)
		{
			gridPositionX = posX;
			gridPositionY = posY;
			targetPosition.x = newRec.x + (texture_base->width * gridPositionX);
			targetPosition.y = newRec.y + (texture_base->height * gridPositionY);
			currentState = MAGIC_SPELL_STATE::UNLOCKED;
		}
		MAGIC_SPELL_STATE MagicSpell::getState()
		{
			return currentState;
		}
		void MagicSpell::setState(MAGIC_SPELL_STATE newState)
		{
			currentState = newState;
		}
		bool MagicSpell::selectToggle()
		{
			if (currentState == MAGIC_SPELL_STATE::SELECTED)
			{
				currentState = MAGIC_SPELL_STATE::LOCKED;
				return false;
			}
			else
			{
				currentState = MAGIC_SPELL_STATE::SELECTED;
				normalColor.a = 255;
				return true;
			}
		}
		bool MagicSpell::posOnRec(Vector2 pos)
		{
			if (pos.x > collider.x&& pos.x < collider.x + collider.width &&
				pos.y > collider.y&& pos.y < collider.y + collider.height)
			{
				return true;
			}
			return false;
		}
		MAGIC_SPELLS_BASIC_TYPES MagicSpell::getType()
		{
			return currentType;
		}
		void MagicSpell::setType(MAGIC_SPELLS_BASIC_TYPES type)
		{
			currentType = type;
		}
		Vector2 MagicSpell::getGridPosition()
		{
			return { static_cast<float>(gridPositionX), static_cast<float>(gridPositionY) };
		}
		void MagicSpell::setSelectedColor(Color color)
		{
			selectedColor = color;
		}
		void MagicSpell::draw()
		{
			if (currentState == MAGIC_SPELL_STATE::SELECTED)
			{
				DrawTexture(*texture_base, static_cast<int>(currentPosition.x), static_cast<int>(currentPosition.y), normalColor);
				DrawTexture(*texture_selected, static_cast<int>(currentPosition.x), static_cast<int>(currentPosition.y), selectedColor);
			}
			else
			{
				DrawTexture(*texture_base, static_cast<int>(currentPosition.x), static_cast<int>(currentPosition.y), normalColor);
			}
#if DEBUG
			DrawRectangleLinesEx(collider, 2, GREEN);
#endif // DEBUG
		}
		void MagicSpell::update()
		{
			if (currentState == MAGIC_SPELL_STATE::UNLOCKED)
			{
				currentPosition.y += FALL_SPEED * GetFrameTime() * screenConfig::currentScreenHeight;
				if(currentPosition.y > targetPosition.y)
				{
					currentPosition.y = targetPosition.y;
					collider.y = currentPosition.y + texture_base->height * COLLIDER_OFFSET;
					currentState = MAGIC_SPELL_STATE::LOCKED;
				}
			}
			else if(currentState == MAGIC_SPELL_STATE::SPAWNING)
			{
				if(normalColor.a + SPAWN_SPEED <= 255)
				{
					normalColor.a += static_cast<unsigned char>(SPAWN_SPEED);
				}
				else
				{
					normalColor.a = 255;
					currentState = MAGIC_SPELL_STATE::LOCKED;
				}
			}
		}

	}
}