#ifndef GRID
#define GRID

#include "gameObjects/spells/magic_spell.h"
#include <vector>

namespace game_objects
{
	namespace grid
	{

		using namespace magic_spell;

		const int GRID_COLUMNS = 10;
		const int GRID_ROWS = 10;
		const Rectangle GRID_REC{ .3f, .45f, .4f, .5f };
		const Vector2 GRID_PAD_OVERSIZE = { .02f ,.03f };
		const Color TRANSPARENT_COLOR = { 255, 255, 255, 0 };
		const float SELECTION_ANIMATION_SPEED = 12.5f;
		const int MIN_SPELLS_FOR_COMBO = 4;
		const char PAUSE_TEXT[] = "Game Paused";
		const Vector2 PAUSE_TEXT_POSITION = { .5f, .7f };
		const float PAUSE_TEXT_SIZE = .05f;
		const Vector2 TEXT_LEVEL_COMPLETE_POSITION = { .505f, .675f };
		const float TEXT_LEVEL_COMPLETE_SIZE = .05f;
		const Vector2 TEXT_GAME_COMPLETE_POSITION = { .505f, .775f };
		const float TEXT_GAME_COMPLETE_SIZE = .02f;

		class Grid
		{
		public:
			Grid();
			~Grid();
			void update();
			void draw();
			void spellsSelectLogic(Vector2 mousePos);
			void spellsDragLogic(Vector2 mousePos);
			void reRollSpellsLogic();
			void spellComboLogic();
			int getComboSize();
			void setLevelComplete();
			void setGameComplete();
			void setGameOver();
			void rollAllSpells();
		private:
			void newSingleSpell(int x, int y);
			void GlobalSelectionAnimationUpdate();
			void firstSpellSelect(int i, int j);
			void replenishSpellsLogic();
			void dragSelectLogic(int i, int j);
			bool rollbackSelectionLogic(int i, int j);
			bool validPiecePosition(int i, int j);
			MAGIC_SPELLS_BASIC_TYPES currentSelectedType;
			MagicSpell* magicSpellsSlots[GRID_COLUMNS][GRID_ROWS] = { 0 };
			std::vector<Vector2>selectedPiecesPositions;
			Rectangle currentRec;
			Color CurrentGlobalSelectedColor = TRANSPARENT_COLOR;
			Texture2D* texture;
			int comboSize = 0;
			bool levelComplete = false;
			bool gameComplete = false;
			bool gameOver = false;
		};
	}
}

#endif

