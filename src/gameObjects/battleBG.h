#ifndef BATTLE_BG_H
#define BATTLE_BG_H

#include "raylib.h"
#include "gameObjects/entities/character.h"
#include "gameObjects/entities/magic_spell_BG.h"


namespace background
{
	const Color TRANSPARENT_COLOR = { 255, 255, 255, 0 };
	const float LEVEL_LOAD_SPEED = 5.f;
	const Rectangle GAME_BG_REC{ .2f, .01f, .6f, .4f };
	const int MAGIC_SPELLS_MAX_AMMOUNT = 50;

	class BattleBG
	{
	public:
		BattleBG();
		~BattleBG();
		void draw();
		void update();
		void lostLevel();
		void startNextLevel();
		void setCurrentLevel(int level);
		void playerCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES type, int damage);
		void enemyCastSpell(game_objects::magic_spell::MAGIC_SPELLS_BASIC_TYPES type, int damage);
	private:
		void setCurrentBGTexture(Texture2D* newBGTexture);
		bool levelOver = false;
		Color currentGameBGColor = TRANSPARENT_COLOR;
		Texture2D* gameBGOverlayTexture = nullptr;
		Texture2D* currentGameBGTexture = nullptr;
		Rectangle currentGameBGRec = { 0, 0, 0, 0 };
		game_objects::entities::Character* player = nullptr;
		game_objects::entities::Character* currentEnemy = nullptr;
		game_objects::entities::BackgroundMagicSpell* playerMagicSpellsPool[MAGIC_SPELLS_MAX_AMMOUNT] = { 0 };
		game_objects::entities::BackgroundMagicSpell* enemyMagicSpellsPool[MAGIC_SPELLS_MAX_AMMOUNT] = { 0 };
	};
}

#endif
