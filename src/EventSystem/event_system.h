#ifndef EVENT_SYSTEM
#define EVENT_SYSTEM

namespace event_system
{

	enum class EVENT_ID { MOVE_MENU, SELECT_MENU, GRID_EVENT,
		ELECTRIC_COMBO, WIND_COMBO, FIRE_COMBO, WATER_COMBO, ICE_COMBO, EARTH_COMBO,
		ELECTRIC_HIT, WIND_HIT, FIRE_HIT, WATER_HIT, ICE_HIT, EARTH_HIT, 
		PLAYER_HIT, WON_LEVEL, LOST_LEVEL };

	class EventSystem
	{
	private:
		bool moveMenuBool;
		bool selectMenuBool;
		bool gridEventBool;
		bool electricComboBool;
		bool electricHitBool;
		bool windComboBool;
		bool windHitBool;
		bool fireComboBool;
		bool fireHitBool;
		bool waterComboBool;
		bool waterHitBool;
		bool iceComboBool;
		bool iceHitBool;
		bool earthComboBool;
		bool earthHitBool;
		bool playerHitBool;
		bool wonLevelBool;
		bool lostLevelBool;
	public:
		EventSystem();
		~EventSystem();
		void setEvent(EVENT_ID eventNumber);
		bool checkEvent(EVENT_ID eventNumber);
		void ResetEvents();
	};

	extern EventSystem* externalEventSystem;

}

#endif