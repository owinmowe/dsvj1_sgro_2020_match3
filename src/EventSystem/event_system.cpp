#include "event_system.h"

namespace event_system
{

	EventSystem* externalEventSystem;

	EventSystem::EventSystem()
	{
		moveMenuBool = false;
		selectMenuBool = false;
		gridEventBool = false;
		electricComboBool = false;
		electricHitBool = false;
		windComboBool = false;
		windHitBool = false;
		fireComboBool = false;
		fireHitBool = false;
		waterComboBool = false;
		waterHitBool = false;
		iceComboBool = false;
		iceHitBool = false;
		earthComboBool = false;
		earthHitBool = false;
		playerHitBool = false;
		wonLevelBool = false;
		lostLevelBool = false;
	}
	EventSystem::~EventSystem()
	{

	}
	void EventSystem::setEvent(EVENT_ID eventNumber)
	{
		switch (eventNumber)
		{
		case event_system::EVENT_ID::MOVE_MENU:
			moveMenuBool = true;
			break;
		case event_system::EVENT_ID::SELECT_MENU:
			selectMenuBool = true;
			break;
		case event_system::EVENT_ID::GRID_EVENT:
			gridEventBool = true;
			break;
		case event_system::EVENT_ID::ELECTRIC_COMBO:
			electricComboBool = true;
			break;
		case event_system::EVENT_ID::WIND_COMBO:
			windComboBool = true;
			break;
		case event_system::EVENT_ID::FIRE_COMBO:
			fireComboBool = true;
			break;
		case event_system::EVENT_ID::WATER_COMBO:
			waterComboBool = true;
			break;
		case event_system::EVENT_ID::ICE_COMBO:
			iceComboBool = true;
			break;
		case event_system::EVENT_ID::EARTH_COMBO:
			earthComboBool = true;
			break;
		case event_system::EVENT_ID::ELECTRIC_HIT:
			electricHitBool = true;
			break;
		case event_system::EVENT_ID::WIND_HIT:
			windHitBool = true;
			break;
		case event_system::EVENT_ID::FIRE_HIT:
			fireHitBool = true;
			break;
		case event_system::EVENT_ID::WATER_HIT:
			waterHitBool = true;
			break;
		case event_system::EVENT_ID::ICE_HIT:
			iceHitBool = true;
			break;
		case event_system::EVENT_ID::EARTH_HIT:
			earthHitBool = true;
			break;
		case event_system::EVENT_ID::PLAYER_HIT:
			playerHitBool = true;
			break;
		case event_system::EVENT_ID::WON_LEVEL:
			wonLevelBool = true;
			break;
		case event_system::EVENT_ID::LOST_LEVEL:
			lostLevelBool = true;
			break;
		default:
			break;
		}
	}
	bool EventSystem::checkEvent(EVENT_ID eventNumber)
	{
		switch (eventNumber)
		{
		case event_system::EVENT_ID::MOVE_MENU:
			return moveMenuBool;
			break;
		case event_system::EVENT_ID::SELECT_MENU:
			return selectMenuBool;
			break;
		case event_system::EVENT_ID::GRID_EVENT:
			return gridEventBool;
			break;
		case event_system::EVENT_ID::ELECTRIC_COMBO:
			return electricComboBool;
			break;
		case event_system::EVENT_ID::WIND_COMBO:
			return windComboBool;
			break;
		case event_system::EVENT_ID::FIRE_COMBO:
			return fireComboBool;
			break;
		case event_system::EVENT_ID::WATER_COMBO:
			return waterComboBool;
			break;
		case event_system::EVENT_ID::ICE_COMBO:
			return iceComboBool;
			break;
		case event_system::EVENT_ID::EARTH_COMBO:
			return earthComboBool;
			break;
		case event_system::EVENT_ID::ELECTRIC_HIT:
			return electricHitBool;
			break;
		case event_system::EVENT_ID::WIND_HIT:
			return windHitBool;
			break;
		case event_system::EVENT_ID::FIRE_HIT:
			return fireHitBool;
			break;
		case event_system::EVENT_ID::WATER_HIT:
			return waterHitBool;
			break;
		case event_system::EVENT_ID::ICE_HIT:
			return iceHitBool;
			break;
		case event_system::EVENT_ID::EARTH_HIT:
			return earthHitBool;
			break;
		case event_system::EVENT_ID::PLAYER_HIT:
			return playerHitBool;
			break;
		case event_system::EVENT_ID::WON_LEVEL:
			return wonLevelBool;
			break;
		case event_system::EVENT_ID::LOST_LEVEL:
			return lostLevelBool;
			break;
		default:
			return false;
			break;
		}
	}
	void EventSystem::ResetEvents()
	{
		moveMenuBool = false;
		selectMenuBool = false;
		gridEventBool = false;
		electricComboBool = false;
		electricHitBool = false;
		windComboBool = false;
		windHitBool = false;
		fireComboBool = false;
		fireHitBool = false;
		waterComboBool = false;
		waterHitBool = false;
		iceComboBool = false;
		iceHitBool = false;
		earthComboBool = false;
		earthHitBool = false;
		playerHitBool = false;
		wonLevelBool = false;
		lostLevelBool = false;
	}
}